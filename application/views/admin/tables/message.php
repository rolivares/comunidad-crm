<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $hasPermissionEdit   = has_permission('tasks', '', 'edit');
// $hasPermissionDelete = has_permission('tasks', '', 'delete');
// $tasksPriorities     = get_tasks_priorities();

$aColumns = [
    'id',
    'name',
    'date',
    'rel_id',
    'rel_type',
    //'(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM ' . db_prefix() . 'taggables JOIN ' . db_prefix() . 'tags ON ' . db_prefix() . 'taggables.tag_id = ' . db_prefix() . 'tags.id WHERE rel_id = ' . db_prefix() . 'message.id and rel_type="message" ORDER by tag_order ASC) as tags',
    ];


$sIndexColumn = 'id';
$sTable       = db_prefix() . 'message';

$result = data_tables_init($aColumns, $sIndexColumn, $sTable);

$output  = $result['output'];
$rResult = $result['rResult'];

//echo "<pre>";  print_r($rResult); die;
foreach ($rResult as $aRow) {
    $row = [];

    $row[] = '<a href="' . admin_url('message/view/' . $aRow['id']) . '" class="display-block main-tasks-table-href-name' . (!empty($aRow['rel_id']) ? ' mbot5' : '') . '" onclick="init_message_modal(' . $aRow['id'] . '); return false;">' . $aRow['id'] . '</a>';

    $outputName = '';

    $outputName .= '<a href="' . admin_url('message/view/' . $aRow['id']) . '" class="display-block main-tasks-table-href-name' . (!empty($aRow['rel_id']) ? ' mbot5' : '') . '" onclick="init_message_modal(' . $aRow['id'] . '); return false;">' . $aRow['name'] . '</a>';

    $outputName .= '<div class="row-options">';

    $class = 'text-success bold';
    $style = '';

    $outputName .= '</div>';

    $row[] = $outputName;

    
    $row[] = $aRow['date'];

    if($aRow['rel_type'] == 'ticket'){
        $url   = admin_url('tickets/ticket/' . $aRow['rel_id']);
        $row[] = '<a href="' . $url . '?tab=settings"># ' .$aRow['rel_id'] . '</a>';
    }else{
        $url   = admin_url('requests/requests/' . $aRow['rel_id']);
        $row[] = '<a href="' . $url . '?tab=settings">' .format_requests_number($aRow['rel_id']) . '</a>';
    }

    if($aRow['rel_type'] == 'ticket'){
        $row[] = '<span class="label s-status" style="border: 1px solid #84c529;color:#84c529;">' ._l('tickets'). '</span>';
    }else{
        $row[] = '<span class="label s-status" style="border: 1px solid #84c529;color:#84c529;">' ._l('requests'). '</span>';
    }

    //$row[] = render_tags($aRow['tags']);

    
    $output['aaData'][] = $row;
}

