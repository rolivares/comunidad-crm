<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionDelete = has_permission('terminal', '', 'delete');

$aColumns = [
    'codaffiliate',
    'serial',
    'mac',
    'imei',
    db_prefix() . 'simcard.serialsimcard as simcard',
    //db_prefix() . 'operadora.operadora as operadora',
    //db_prefix() . 'banks.banks as banks',
    'numterminal',
    ];

    $join = [
        //'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.id = ' . db_prefix() . 'terminals.banks',
        //'LEFT JOIN ' . db_prefix() . 'operadora ON ' . db_prefix() . 'operadora.operadoraid = ' . db_prefix() . 'terminals.operadoraid',
        'LEFT JOIN ' . db_prefix() . 'simcard ON ' . db_prefix() . 'simcard.id = ' . db_prefix() . 'terminals.simcard',
    ];
    

$where = [];
if ($clientid != '') {
    array_push($where, 'AND ' . db_prefix() . 'terminals.userid=' . $this->ci->db->escape_str($clientid));
}



$sIndexColumn = 'id';
$sTable       = db_prefix() . 'terminals';

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where,[db_prefix() . 'terminals.id as id', 'userid']);

$output  = $result['output'];

$rResult = $result['rResult'];
//var_dump($rResult); die;

foreach ($rResult as $aRow) {
    $row = [];

    $rowName =  $aRow['codaffiliate'];

    $rowName .= '<div class="row-options">';

    //$rowName .= '<a href="#" onclick="terminals(' . $aRow['userid'] . ',' . $aRow['id'] . ');return false;">' . _l('edit') . '</a>';

    $rowName .= '</div>';


    $row[] = $rowName;

    $row[] = $aRow['serial'];

    $row[] = $aRow['mac'];

    $row[] = $aRow['imei'];

    $row[] = $aRow['simcard'];

    //$row[] = $aRow['operadora'];

    //$row[] = $aRow['banks'];
    
    
    $row[] = $aRow['numterminal'];

    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}
