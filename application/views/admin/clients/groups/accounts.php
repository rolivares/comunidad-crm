<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if(isset($client)){ ?>
<h4 class="customer-profile-group-heading"><?php echo is_empty_customer_company($client->userid) ? _l('accounts') : _l('accounts'); ?></h4>
<?php if($this->session->flashdata('gdpr_delete_warning')){ ?>
    <div class="alert alert-warning">
     [GDPR] The contact you removed has associated proposals using the email address of the contact and other personal information. You may want to re-check all proposals related to this customer and remove any personal data from proposals linked to this contact.
   </div>
<?php } ?>
<?php if((has_permission('customers','','create') || is_customer_admin($client->userid)) && $client->registration_confirmed == '1'){
   $disable_new_contacts = false;
   if(is_empty_customer_company($client->userid) && total_rows(db_prefix().'contacts',array('userid'=>$client->userid)) == 1){
      $disable_new_contacts = true;
   }
   ?>
<div class="inline-block new-contact-wrapper" data-title="<?php echo _l('customer_contact_person_only_one_allowed'); ?>"<?php if($disable_new_contacts){ ?> data-toggle="tooltip"<?php } ?>>
   <a href="#" onclick="accounts(<?php echo $client->userid; ?>); return false;" class="btn btn-info new-terminal mbot25<?php if($disable_new_contacts){echo ' disabled';} ?>"><?php echo _l('new_accounts'); ?></a>
</div>
<?php } ?>
<?php
   
  $table_data = array_merge(array( _l('Codigo de afiliado'),_l('Banco'),_l('Estatus'), _l('Nº de cuenta'), _l('Fecha')));
   $custom_fields = get_custom_fields('accounts-single-client',array('show_on_table'=>1));
   foreach($custom_fields as $field){
      array_push($table_data,$field['name']);
   }
   echo render_datatable($table_data,'accounts-single-client'); ?> 
<?php } ?>
<div id="accounts_data"></div>
<div id="accounts_data"></div>
