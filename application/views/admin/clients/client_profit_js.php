<script>

function setclient() {


        var formData = new FormData();

        var number_rif = $("#type_rif").val() + $("#rif_number").val();

        if (typeof(csrfData) !== 'undefined') {
            formData.append(csrfData['token_name'], csrfData['hash']);
        }
        formData.append("number_rif", number_rif);

        console.log(formData);
        $.ajax({
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            url: admin_url + 'clients/setclient',
            dataType: 'json'
        }).done(function(response){
                
                if (response.success) {
                    console.log(response.success);
                    alert_float('success', response.message);
                
                }
        }).fail(function(error){
            alert_float('danger', JSON.parse(error.responseText));
            console.log("error");
        });
   
}

</script>