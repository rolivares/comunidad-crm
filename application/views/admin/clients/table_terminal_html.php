<?php defined('BASEPATH') or exit('No direct script access allowed');

render_datatable([
    _l('codigo de afiliado'),
    _l('serial'),
    _l('mac'),
    _l('imei'),
    _l('simcard'),
    //_l('operadora'),
    //_l('banco'),
], (isset($class) ? $class : 'terminal'), [], [
    'data-last-order-identifier' => 'terminal',
    'data-default-order'         => get_table_last_order('terminal'),
]);
