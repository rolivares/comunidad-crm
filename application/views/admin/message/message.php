<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open_multipart(admin_url('message/message/'.$id),array('id'=>'message-form')); ?>
<div class="modal fade<?php if(isset($task)){echo ' edit';} ?>" id="_task_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"<?php if($this->input->get('opened_from_lead_id')){echo 'data-lead-id='.$this->input->get('opened_from_lead_id'); } ?>>
<div class="modal-dialog" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel">
            <?php echo _l('add_message'); ?>
         </h4>
      </div>
      <div class="modal-body">
         <div class="row">
            <div class="col-md-12">
               <?php
                  $rel_type = '';
                  $rel_id = '';
                  if(isset($task) || ($this->input->get('rel_id') && $this->input->get('rel_type'))){
                      $rel_id = isset($task) ? $task->rel_id : $this->input->get('rel_id');
                      $rel_type = isset($task) ? $task->rel_type : $this->input->get('rel_type');
                   }
                   if(isset($task) && $task->billed == 1){
                     echo '<div class="alert alert-success text-center no-margin">'._l('task_is_billed','<a href="'.admin_url('invoices/list_invoices/'.$task->invoice_id).'" target="_blank">'.format_invoice_number($task->invoice_id)). '</a></div><br />';
                   }
                  ?>
              
               
               <?php $value = (isset($task) ? $task->name : ''); ?>
               <?php echo render_input('name','theme_message',$value);?>
              
             
               <div class="recurring_custom <?php if((isset($task) && $task->custom_recurring != 1) || (!isset($task))){echo 'hide';} ?>">
                  <div class="row">
                     <div class="col-md-6">
                        <?php $value = (isset($task) && $task->custom_recurring == 1 ? $task->repeat_every : 1); ?>
                        <?php echo render_input('repeat_every_custom','',$value,'number',array('min'=>1)); ?>
                     </div>
                     <div class="col-md-6">
                        <select name="repeat_type_custom" id="repeat_type_custom" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <option value="day" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'day'){echo 'selected';} ?>><?php echo _l('task_recurring_days'); ?></option>
                           <option value="week" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'week'){echo 'selected';} ?>><?php echo _l('task_recurring_weeks'); ?></option>
                           <option value="month" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'month'){echo 'selected';} ?>><?php echo _l('task_recurring_months'); ?></option>
                           <option value="year" <?php if(isset($task) && $task->custom_recurring == 1 && $task->recurring_type == 'year'){echo 'selected';} ?>><?php echo _l('task_recurring_years'); ?></option>
                        </select>
                     </div>
                  </div>
               </div>
               <div id="cycles_wrapper" class="<?php if(!isset($task) || (isset($task) && $task->recurring == 0)){echo ' hide';}?>">
                  <?php $value = (isset($task) ? $task->cycles : 0); ?>
                  <div class="form-group recurring-cycles">
                     <label for="cycles"><?php echo _l('recurring_total_cycles'); ?>
                     <?php if(isset($task) && $task->total_cycles > 0){
                        echo '<small>' . _l('cycles_passed', $task->total_cycles) . '</small>';
                        }
                        ?>
                     </label>
                     <div class="input-group">
                        <input type="number" class="form-control"<?php if($value == 0){echo ' disabled'; } ?> name="cycles" id="cycles" value="<?php echo $value; ?>" <?php if(isset($task) && $task->total_cycles > 0){echo 'min="'.($task->total_cycles).'"';} ?>>
                        <div class="input-group-addon">
                           <div class="checkbox">
                              <input type="checkbox"<?php if($value == 0){echo ' checked';} ?> id="unlimited_cycles">
                              <label for="unlimited_cycles"><?php echo _l('cycles_infinity'); ?></label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="rel_type" class="control-label"><?php echo _l('task_related_to'); ?></label>
                        <select name="rel_type" class="selectpicker" id="rel_type" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <option value=""></option>
                           
                              <option value="ticket" <?php if(isset($task) || $this->input->get('rel_type')){if($rel_type == 'ticket'){echo 'selected';}} ?>>
                                 <?php echo _l('ticket'); ?>
                              </option>
                              <option value="requests" <?php if(isset($task) || $this->input->get('rel_type')){if($rel_type == 'requests'){echo 'selected';}} ?>>
                                 <?php echo _l('requests'); ?>
                              </option>
                          
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group<?php if($rel_id == ''){echo ' hide';} ?>" id="rel_id_wrapper">
                        <label for="rel_id" class="control-label"><span class="rel_id_label"></span></label>
                        <div id="rel_id_select">
                           <select name="rel_id" id="rel_id" class="ajax-sesarch" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                           <?php if($rel_id != '' && $rel_type != ''){
                              $rel_data = get_relation_data($rel_type,$rel_id);
                              $rel_val = get_relation_values($rel_data,$rel_type);
                              echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
                              } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
              
               <?php $rel_id_custom_field = (isset($task) ? $task->id : false); ?>
               <?php echo render_custom_fields('tasks',$rel_id_custom_field); ?>
               <hr />
               <p class="bold"><?php echo _l('message_add_edit_description'); ?></p>
               <?php
               // onclick and onfocus used for convert ticket to task too
               echo render_textarea('description','',(isset($task) ? $task->description : ''),array('rows'=>6,'placeholder'=>_l('task_add_description'),'data-task-ae-editor'=>true, !is_mobile() ? 'onclick' : 'onfocus'=>(!isset($task) || isset($task) && $task->description == '' ? 'init_editor(\'.tinymce-task\', {height:200, auto_focus: true});' : '')),array(),'no-mbot','tinymce-task'); ?>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
         <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
      </div>
   </div>
</div>
<?php echo form_close(); ?>
<script>
   var _rel_id = $('#rel_id'),
   _rel_type = $('#rel_type'),
   _rel_id_wrapper = $('#rel_id_wrapper'),
   data = {};

   var _milestone_selected_data;
   _milestone_selected_data = undefined;

   $(function(){

    $( "body" ).off( "change", "#rel_id" );

    var inner_popover_template = '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"></div></div></div>';

    $('#_task_modal .task-menu-options .trigger').popover({
      html: true,
      placement: "bottom",
      trigger: 'click',
      title:"<?php echo _l('actions'); ?>",
      content: function() {
       return $('body').find('#_task_modal .task-menu-options .content-menu').html();
     },
     template: inner_popover_template
   });

    custom_fields_hyperlink();

    appValidateForm($('#message-form'), {
      name: 'required',
      repeat_every_custom: { min: 1},
    },message_form_handler);

    $('.rel_id_label').html(_rel_type.find('option:selected').text());

    _rel_type.on('change', function() {
      
     var clonedSelect = _rel_id.html('').clone();
     _rel_id.selectpicker('destroy').remove();
     _rel_id = clonedSelect;
     $('#rel_id_select').append(clonedSelect);
     $('.rel_id_label').html(_rel_type.find('option:selected').text());

     task_rel_select();
     if($(this).val() != ''){
      _rel_id_wrapper.removeClass('hide');
    } else {
      _rel_id_wrapper.addClass('hide');
    }
    init_project_details(_rel_type.val());
   });

    init_datepicker();
    init_color_pickers();
    init_selectpicker();
    task_rel_select();

    $('body').on('change','#rel_id',function(){
     if($(this).val() != ''){
       if(_rel_type.val() == 'project'){
         $.get(admin_url + 'projects/get_rel_project_data/'+$(this).val()+'/'+taskid,function(project){
           $("select[name='milestone']").html(project.milestones);
           if(typeof(_milestone_selected_data) != 'undefined'){
            $("select[name='milestone']").val(_milestone_selected_data.id);
            $('input[name="duedate"]').val(_milestone_selected_data.due_date)
          }
          $("select[name='milestone']").selectpicker('refresh');
          if(project.billing_type == 3){
           $('.task-hours').addClass('project-task-hours');
         } else {
           $('.task-hours').removeClass('project-task-hours');
         }

         if(project.deadline) {
            var $duedate = $('#_task_modal #duedate');
            var currentSelectedTaskDate = $duedate.val();
            $duedate.attr('data-date-end-date', project.deadline);
            $duedate.datetimepicker('destroy');
            init_datepicker($duedate);

            if(currentSelectedTaskDate) {
               var dateTask = new Date(unformat_date(currentSelectedTaskDate));
               var projectDeadline = new Date(project.deadline);
               if(dateTask > projectDeadline) {
                  $duedate.val(project.deadline_formatted);
               }
            }
         } else {
            reset_task_duedate_input();
         }
         init_project_details(_rel_type.val(),project.allow_to_view_tasks);
       },'json');
       } else {
         reset_task_duedate_input();
       }
     }
   });

    <?php if(!isset($task) && $rel_id != ''){ ?>
      _rel_id.change();
      <?php } ?>

    });

   <?php if(isset($_milestone_selected_data)){ ?>
    _milestone_selected_data = '<?php echo json_encode($_milestone_selected_data); ?>';
    _milestone_selected_data = JSON.parse(_milestone_selected_data);
    <?php } ?>

    function task_rel_select(){
      var serverData = {};
      serverData.rel_id = _rel_id.val();
      data.type = _rel_type.val();
      init_ajax_search(_rel_type.val(),_rel_id,serverData);
     }

     function init_project_details(type,tasks_visible_to_customer){
      var wrap = $('.non-project-details');
      var wrap_task_hours = $('.task-hours');
      if(type == 'project'){
        if(wrap_task_hours.hasClass('project-task-hours') == true){
          wrap_task_hours.removeClass('hide');
        } else {
          wrap_task_hours.addClass('hide');
        }
        wrap.addClass('hide');
        $('.project-details').removeClass('hide');
      } else {
        wrap_task_hours.removeClass('hide');
        wrap.removeClass('hide');
        $('.project-details').addClass('hide');
        $('.task-visible-to-customer').addClass('hide').prop('checked',false);
      }
      if(typeof(tasks_visible_to_customer) != 'undefined'){
        if(tasks_visible_to_customer == 1){
          $('.task-visible-to-customer').removeClass('hide');
          $('.task-visible-to-customer input').prop('checked',true);
        } else {
          $('.task-visible-to-customer').addClass('hide')
          $('.task-visible-to-customer input').prop('checked',false);
        }
      }
    }
    function reset_task_duedate_input() {
      var $duedate = $('#_task_modal #duedate');
      $duedate.removeAttr('data-date-end-date');
      $duedate.datetimepicker('destroy');
      init_datepicker($duedate);
   }
</script>
