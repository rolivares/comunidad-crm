<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="panel_s">
			<div class="panel-body">
				<div class="col-md-12">
					<a href="<?php echo admin_url('invoices/create_tasa'); ?>" class="btn btn-info pull-left display-block mright5"><?php echo _l('new_rate'); ?></a>
				</div> 
				<div class="clearfix"></div>
				<hr class="hr-panel-heading" />
				<div class="col-md-12">
					<?php $this->load->view('admin/rate/table_html'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
<script>
	$(function(){
		initDataTable('.table-rate', admin_url+'rate/table', undefined, undefined,'undefined',<?php echo hooks()->apply_filters('payments_table_default_order', json_encode(array(0,'desc'))); ?>);
	});
</script>
</body>
</html>
