<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
  $file_header = array();
$file_header[] = _l('model');
//$file_header[] = _l('Affiliate_code');
$file_header[] = _l('serial');
$file_header[] = _l('mac');
$file_header[] = _l('imei');
//$file_header[] = _l('simcard');
//$file_header[] = _l('operator');

//$file_header[] = _l('status');
//$file_header[] = _l('bank');


 ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div id ="dowload_file_sample">
                            <a href="<?php echo base_url('uploads/import_excel/sample_import_excel.csv'); ?>" class="btn btn-info pull-left display-block mright5 hidden-xs" download="sample_import_excel.csv">
                                    <?php echo _l('download_excel'); ?>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php if(!isset($simulate)) { ?>
                            <ul>
                            <li class="text-danger">1. <?php echo _l('detail_one'); ?></li>
                            <li class="text-danger">2. <?php echo _l('detail_two'); ?></li>
                            <li class="text-danger">3. <?php echo _l('detail_three'); ?></li>
                           <li class="text-danger">4. <?php echo _l('detail_four'); ?></li> 
                            </ul>
                            <div class="table-responsive no-dt">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <?php
                                    $total_fields = 0;
                                    
                                    for($i=0;$i<count($file_header);$i++){
                                        
                                        ?>
                                        <th class="bold"><?php echo html_entity_decode($file_header[$i]) ?> </th>
                                        <?php
                                        $total_fields++;
                                    }

                                    ?>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php for($i = 0; $i<1;$i++){
                                    echo '<tr>';
                                    for($x = 0; $x<count($file_header);$x++){
                                        echo '<td>-</td>';
                                    }
                                    echo '</tr>';
                                    }
                                    ?>
                                </tbody>
                                </table>
                            </div>
                            <hr>

                            <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'import_form')) ;?>
									<div class="col-md-6">
                                        <?php echo form_hidden('clients_import','true'); ?>
                                        <?php echo render_input('file_csv','choose_csv_file','','file'); ?>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group select-placeholder" id="ticket_contact_w">
                                            <label for="contactid"><?php echo _l('contact'); ?></label>
                                            <select name="contactid" required="true" id="contactid"
                                                class="ajax-search search-contactid" data-width="100%" data-abs-cache="false"
                                                data-live-search="true"
                                                data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                <?php if(isset($contact)) { ?>
                                                <option value="<?php echo $contact['id']; ?>" selected>
                                                    <?php echo $contact['firstname'] . ' ' .$contact['lastname']; ?></option>
                                                <?php } ?>
                                                <option value=""></option>
                                            </select>
                                            <?php echo form_hidden('userid'); ?>
                                        </div>
                                     </div>
                                      <!--<div class="col-md-6">
                                        <?php echo render_input('name_user','ticket_settings_to','','text',array('required'=>'true')); ?>
                                     </div>
                                     <div class="col-md-6">
                                        <?php echo render_input('email','ticket_settings_email','','email',array('required'=>'true')); ?>    
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo render_input('rif_number_user','RIF','','text',array('required'=>'true')); ?>
                                    </div> -->
                                    <?php echo form_hidden('name_user'); ?>
                                    <?php echo form_hidden('email'); ?>
                                    <?php echo form_hidden('rif_number_user'); ?>
                                    <div class="col-md-6">
										<?php echo render_select('department',$departments,array('departmentid','name'),'ticket_settings_departments',(count($departments) == 1) ? $departments[0]['departmentid'] : '',array('required'=>'true'),array(),'','',false); ?>
									</div>
									<div class="col-md-6">
                                        <?php echo render_select('banks',$banks,array('id','banks'),'Banco',array('required'=>true));?>
                                    </div>
                                    <div class="col-md-6">
										<div class="form-group select-placeholder">
											<label for="assigned" class="control-label">
												<?php echo _l('ticket_settings_assign_to'); ?>
											</label>
											<select name="assigned" id="assigned" class="form-control selectpicker" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-width="100%">
												<option value=""><?php echo _l('ticket_settings_none_assigned'); ?></option>
												<?php foreach($staff as $member){ ?>
													<option value="<?php echo $member['staffid']; ?>" <?php if($member['staffid'] == get_staff_user_id()){echo 'selected';} ?>>
														<?php echo $member['firstname'] . ' ' . $member['lastname'] ; ?>
													</option>
												<?php } ?>
											</select>
										</div>
                                    </div>
                                    <div class="col-md-6">
										<?php $priorities['callback_translate'] = 'ticket_priority_translate'; echo render_select('priority', $priorities, array('priorityid','name'), 'ticket_settings_priority', hooks()->apply_filters('new_ticket_priority_selected', 2), array('required'=>'true')); ?>
                                    </div>
                                    <div class="col-md-6">
										<?php if(get_option('services') == 1): ?>
											<?php if(is_admin() || get_option('staff_members_create_inline_ticket_services') == '1'){
												echo render_select_with_input_group('service',$services,array('serviceid','name'),'type_request','','<a href="#" onclick="new_service();return false;"><i class="fa fa-plus"></i></a>');
											} else {
												echo render_select('service',$services,array('serviceid','name'),'ticket_settings_service');
											}
											?>
										<?php endif ?>
									</div>
                                    
                                    <div class="clearfix"></div>
                                    <hr class="hr-panel-heading" />

                                    <div class="form-group">
                                        <button type="button"
                                            class="btn btn-info import btn-import-submit"><?php echo _l('import'); ?></button>
                                        <!-- <button type="button" class="btn btn-info simulate btn-import-submit"><?php echo _l('simulate_import'); ?></button> -->
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>

<script>
$(function() {
    appValidateForm($('#import_form'),{file_csv:{required:true,extension: "csv"},source:'required',status:'required'});  
    var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
    init_ajax_search('contact', '#contactid.ajax-search', {
        tickets_contacts: true,
        contact_userid: function() {
            // when ticket is directly linked to project only search project client id contacts
            var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
            if (uid) {
                return uid;
            } else {
                return '';
            }
        }
    });

    $(".ajax-search .dropdown-menu").click(function() {
        var e;
        var t = $('select[name="contactid"]').val();
        var a = $('select[name="project_id"]');
        var i = a.attr("data-auto-project");
        var n = $(".projects-wrapper");

        var stock_name = [];

        $("#tableticketsresumen tbody tr").each(function(index) {
            if (index >= 0) {
                $('#terminales').html('');
                $('#table').removeClass('shadow-z-1');
                i = 0;
            }
        });

        a.attr("disabled") || (i ? (e = a.clone()).prop("disabled", !0) : (e = a.html("").clone()),
            a.selectpicker("destroy").remove(),
            (a = e),
            $("#project_ajax_search_wrapper").append(e),
            init_ajax_search("project", a, {
                customer_id: function() {
                    return $('input[name="userid"]').val();
                },
            })
        );
        if (t != '') {
            $.post(admin_url + "tickets/ticket_change_data/", {
                contact_id: t
            }).done(function(e) {
                var res = JSON.parse(e).contact_data;
                $('input[name="name_user"]').val(res.firstname + " " + res.lastname);
                $('input[name="email"]').val(res.email);
                $('input[name="userid_user"]').val(res.userid);
                $('input[name="rif_number_user"]').val(res.rif_number);
                //$('input[name="codaffiliate_user"]').val(res.codaffiliate);
                $('input[name="codaffiliate_user"]').val(res.codeaffiliate);
                $('input[name="userid"]').val(res.userid);
                //AjaxTerminal(res.userid);
                if (res.ticket_emails == "0") {
                    show_ticket_no_contact_email_warning(res.userid, res.id);
                } else {
                    //clear_ticket_no_contact_email_warning()
                }
                (i) ? n.removeClass("hide"): res.customer_has_projects ? n.removeClass("hide") :
                    n.addClass("hide");
            })
        }
    });



});
</script>

</html>