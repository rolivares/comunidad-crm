<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>

<div id="wrapper">
    <div class="content">
        <?php echo form_open_multipart($this->uri->uri_string(), array('id' => 'requests-form')); ?>
        <div class="col-md">
            <div class="panel_s">
                <div class="panel-body">
                    <div>
                        <div class="tab-content">
                            <h4 class="customer-profile-group-heading"><?php echo _l('new_requests'); ?></h4>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo _l('type_requests'); ?></label>
                                    <div class="form-group select-placeholder" id="type_requests">
                                        <select name="type_operation" id="type_operation" required="true" data-width="100%" data-live-search="true" class="selectpicker model type_operation" data-title="<?php echo _l('Nada seleccionado'); ?>">
                                            <?php foreach ($type_operation as $type_operation) { ?>
                                                <option value="<?php echo $type_operation['id_type']; ?>">
                                                    <?php echo $type_operation['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <label for="contactid"><?php echo _l('contact'); ?></label>
                                    <div class="form-group select-placeholder" id="ticket_contact_w">
                                        <select name="contactid" required="true" id="contactid" class="ajax-search search-contactid" data-width="100%" data-abs-cache="false" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                            <?php if (isset($contact)) { ?>
                                                <option value="<?php echo $contact['id']; ?>" selected>
                                                    <?php echo $contact['firstname'] . ' ' . $contact['lastname']; ?>
                                                </option>
                                            <?php } ?>
                                            <option value=""></option>
                                        </select>
                                        <?php echo render_input('userid', 'userid', '', 'hidden'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label for="chanel"><?php echo _l('requests_chanel'); ?></label>
                                    <?php echo render_select_terminal('chanel', 'chanel', $chanels, array('id', 'name'), _l('chanel'), (isset($chanels) ? $chanels : '')); ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-6" id="requests_status">
                                    <label for="requests_status"><?php echo _l('requests_status'); ?></label>
                                    <?php echo render_select_terminal('requests_status', 'requests_status', $status_add, array('id_status', 'name'), _l('requests_status'), (isset($status_add) ? $status_add : '1'), array('required' => true)); ?>
                                </div>
                                <div class="col-md-6" style='display: none' id="requests_status_other">
                                    <label for="requests_status"><?php echo _l('requests_status'); ?></label>
                                    <?php echo render_select_terminal('requests_status_one', 'requests_status_one', $status, array('id_status', 'name'), _l('requests_status'), (isset($status_add) ? $status_add : '1'), array('required' => true)); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo render_date_input('date', 'requests_date', date('d-m-Y'), array('required' => true)); ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div id='form'>
                                    <div class="col-md-3">
                                        <?php echo render_input('rif', 'requests_rif', '', 'text', array('disabled' => 'true')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php echo render_input('business_name', 'type_requests_razon_social', '', 'text', array('disabled' => 'true')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php echo render_input('requests_phone', 'requests_phone', '', 'number', array('disabled' => 'true')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php echo render_input('requests_affiliate_code', 'requests_affiliate_code', '', 'number'); ?>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="requests_banks"><?php echo _l('requests_banks'); ?></label>
                                        <?php echo render_select_terminal('requests_banks', 'requests_banks', $banks, array('id', 'banks'), _l('requests_banks'), (isset($banks) ? $banks : '')); ?>
                                    </div>
                                    <div class="col-md-3" id="serial" style='display: none'>
                                        <div class="terminales form-group form-group-select-input-service input-group-select">
                                            <label for="terminal"><?php echo _l('requests_serial'); ?></label>
                                            <div class='input-group input-group-select select-terminal'>
                                                <select name="terminal" id="terminal" class="ajax-search-terminal selectpicker jqTransformSelectWrapper" data-width="100%" data-live-search="true" data-abs-cache="false" data-abs-bind-event="click" data-abs-preserve-selected="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" disabled>
                                                    <option value=""></option>
                                                </select>
                                                <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                    <a href="#"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="serial_simcard" style='display: none'>
                                        <div class="col-md-3">
                                            <div class="terminales_simcard form-group form-group-select-input-service input-group-select">
                                                <label for="terminal"><?php echo _l('requests_serial'); ?></label>
                                                <div class='input-group input-group-select select-terminal'>
                                                    <select name="terminal_simcard" id="terminal_simcard" class="ajax-search-terminal selectpicker jqTransformSelectWrapperSimcard" data-width="100%" data-live-search="true" data-abs-cache="false" data-abs-bind-event="click" data-abs-preserve-selected="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" disabled>
                                                        <option value=""></option>
                                                    </select>
                                                    <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                        <a href="#"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" app-field-wrapper="serialsimcard">
                                                <label for="serialsimcard" class="control-label"><?php echo _l('sim_card_serial'); ?></label>
                                                <select name="simcard" required="true" id="simcard" class="ajax-search simcard" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                    <option value=""></option>
                                                </select>
                                                <p id="serialsimcard-error" class="text-danger" style="display: none;"> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3" id="serial_install" style='display: none'>
                                        <div>
                                            <div id="additional"><?php echo _l('requests_serial'); ?></div>
                                            <?php echo render_select_terminal('requests_serial_install', 'requests_serial_install', $lisTerminals, array('id', 'serial'), 'Banco', (isset($terminal) ? $terminal['serial'] : '')); ?>
                                        </div>
                                    </div>

                                    <div id="correctivo" style='display: none'>
                                        <div class="col-md-3">
                                            <div class="terminales_correctivo form-group form-group-select-input-service input-group-select">
                                                <label for="terminal"><?php echo _l('requests_serial_outgoing'); ?></label>
                                                <div class='input-group input-group-select select-terminal'>
                                                    <select name="terminal_correctivo" id="terminal_correctivo" class="ajax-search-terminal selectpicker jqTransformSelectWrapperCorrectivo" data-width="100%" data-live-search="true" data-abs-cache="false" data-abs-bind-event="click" data-abs-preserve-selected="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" disabled>
                                                        <option value=""></option>
                                                    </select>
                                                    <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                        <a href="#"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div id="additional"><?php echo _l('requests_serial_incoming'); ?></div>
                                            <?php echo render_select_terminal('requests_serial_incoming', 'requests_serial', $lisTerminals, array('id', 'serial'), 'Banco', (isset($terminal) ? $terminal['serial'] : '')); ?>
                                        </div>
                                        <!-- campos ocultos -->
                                        <?php echo render_input('terminal_saliente', 'terminal_saliente', '', 'hidden'); ?>
                                        <?php echo render_input('terminal_entrante', 'terminal_entrante', '', 'hidden'); ?>
                                    </div>

                                    <div class="col-md-3">
                                        <?php echo render_input('num_terminal', 'requests_terminals', '', 'number'); ?>
                                    </div>


                                    <div id='social_reason_change' style='display: none'>

                                        <div class="col-md-12">
                                            <h5><?php echo _l('type_requests_new_razon_social'); ?></h5>
                                        </div>
                                        <div class="col-md-1">
                                            <?php echo render_select('type_rif', $type_rif, array('type_rif', 'type_rif'), 'type_rif'); ?>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo render_input('rif_reason_change', 'rif_number', '', 'number'); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('business_name_reason_change', 'type_requests_razon_social', '', 'text'); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('phone_reason_change', 'requests_phone', '', 'text'); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('affiliate_code_reason_change', 'requests_affiliate_code', '', 'number'); ?>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="requests_banks"><?php echo _l('requests_banks'); ?></label>
                                            <?php echo render_select_terminal('requests_banks_reason_change', 'requests_banks_reason_change', $banks, array('id', 'banks'), _l('requests_banks'), (isset($banks) ? $banks : '')); ?>
                                        </div>

                                        <div class="col-md-3">
                                            <?php echo render_input('num_terminal_reason_change', 'terminal_reason_change', '', 'number'); ?>
                                        </div>

                                    </div>
                                    <div id='bank_change' style='display: none'>
                                        <div class="col-md-12">
                                            <h5><?php echo _l('type_requests_new_cambio_banco'); ?></h5>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('rif_bank_change', 'rif_bank_change', '', 'text', array('readonly' => 'true')); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('business_name_bank_change', 'business_name_bank_change', '', 'text', array('readonly' => 'true')); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('phone_bank_change', 'phone_bank_change', '', 'text', array('readonly' => 'true')); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('affiliate_code_bank_change', 'requests_affiliate_code', '', 'number'); ?>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="requests_banks"><?php echo _l('requests_banks'); ?></label>
                                            <?php echo render_select_terminal('requests_banks_change', 'requests_banks_change', $banks, array('id', 'banks'), _l('requests_banks'), (isset($banks) ? $banks : '')); ?>
                                        </div>

                                        <div class="col-md-3">
                                            <?php echo render_input('num_terminal_banks_change', 'requests_terminals', '', 'number'); ?>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <?php echo render_textarea('observation', 'requests_observation'); ?>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info" id="submit"><?php echo _l('submit'); ?></button>
                </div>
            </div>

        </div>
        <?php echo form_close(); ?>
        <?php init_tail(); ?>

    </div>
    <script>
        $(function() {


            $('#requests-form').validate({


                rules: {
                    name: 'required',
                    type_operation: 'required',
                    contactid: 'required',
                    phone_reason_change: {
                        maxlength: 11
                    }
                },
                submitHandler: function save_requests() {

                    var terminal_saliente = $('#terminal_correctivo').val();
                    $('#terminal_saliente').val(terminal_saliente);

                    var terminal_entrante = $('#requests_serial_incoming').val();
                    $('#terminal_entrante').val(terminal_entrante);

                    let value = $('#requests-form').serializeArray();

                    $.ajax({
                        method: 'post',
                        data: value,
                        url: admin_url + 'requests/requests',
                        dataType: 'json'

                    }).done(function(response) {
                        if (response.status) {
                            window.location.href = admin_url + 'requests';
                            alert_float('success', response.message);
                        }
                        $("#submit").prop("disabled", true);
                    }).fail(function(error) {
                        alert_float('danger', JSON.parse(error.responseText));

                    });

                }
            })


            var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
            init_ajax_search('contact', '#contactid.ajax-search', {
                tickets_contacts: true,
                contact_userid: function() {
                    var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
                    if (uid) {
                        return uid;
                    } else {
                        return '';
                    }
                }
            });

            $(".ajax-search .dropdown-menu").click(function() {
                var e;
                var t = $('select[name="contactid"]').val();
                var a = $('select[name="project_id"]');
                var i = a.attr("data-auto-project");
                var n = $(".projects-wrapper");
                var stock_name = [];

                if (t != '') {
                    $.post(admin_url + "requests/requests_change_data/", {
                        contact_id: t
                    }).done(function(e) {
                        var res = JSON.parse(e).contact_data;
                        var type = $('#type_operation').val();
                        var cas = {
                            '1': () => { //activacion
                                console.log("caso 1 ")
                                $('input[name="rif_bank_change"]').val('');
                                $('input[name="business_name_bank_change"]').val('');
                                $('input[name="phone_bank_change"]').val('');
                                $('input[name="affiliate_code_bank_change"]').val('');
                                $('input[name="requests_banks_change"]').val('');
                                $('input[name="terminal_banks_change"]').val('');
                                $('input[name="num_terminal_banks_change"]').val('');
                                $('input[name="rif_reason_change"]').val('');
                                $('input[name="business_name_reason_change"]').val('');
                                $('input[name="phone_reason_change"]').val('');
                                $('input[name="affiliate_code_reason_change"]').val('');
                                $('input[name="requests_banks_reason_change"]').val('');
                                $('input[name="terminal_reason_change"]').val('');
                                $('input[name="num_terminal_reason_change"]').val('');
                                $('input[name="rif"]').val(res.rif_number);
                                $('input[name="userid"]').val(res.userid);
                                $('input[name="business_name"]').val(res.company);
                                $('input[name="requests_phone"]').val(res.phonenumber);
                            },
                            '2': () => { // reactivacion
                                console.log("caso 2 ")
                                $('input[name="rif_bank_change"]').val('');
                                $('input[name="business_name_bank_change"]').val('');
                                $('input[name="phone_bank_change"]').val('');
                                $('input[name="affiliate_code_bank_change"]').val('');
                                $('input[name="requests_banks_change"]').val('');
                                $('input[name="terminal_banks_change"]').val('');
                                $('input[name="num_terminal_banks_change"]').val('');
                                $('input[name="rif_reason_change"]').val('');
                                $('input[name="business_name_reason_change"]').val('');
                                $('input[name="phone_reason_change"]').val('');
                                $('input[name="affiliate_code_reason_change"]').val('');
                                $('input[name="requests_banks_reason_change"]').val('');
                                $('input[name="terminal_reason_change"]').val('');
                                $('input[name="num_terminal_reason_change"]').val('');
                                $('input[name="rif"]').val(res.rif_number);
                                $('input[name="userid"]').val(res.userid);
                                $('input[name="business_name"]').val(res.company);
                                $('input[name="requests_phone"]').val(res.phonenumber);
                            },
                            '3': () => { // cambio de razon social
                                console.log("caso 3")
                                $('input[name="rif_bank_change"]').val('');
                                $('input[name="business_name_bank_change"]').val('');
                                $('input[name="phone_bank_change"]').val('');
                                $('input[name="affiliate_code_bank_change"]').val('');
                                $('input[name="requests_banks_change"]').val('');
                                $('input[name="terminal_banks_change"]').val('');
                                $('input[name="num_terminal_banks_change"]').val('');
                                $('input[name="rif"]').val(res.rif_number);
                                $('input[name="userid"]').val(res.userid);
                                $('input[name="business_name"]').val(res.company);
                                $('input[name="requests_phone"]').val(res.phonenumber);
                            },
                            '4': () => { //cambio de banco
                                console.log("caso 4")
                                $('input[name="rif_reason_change"]').val('');
                                $('input[name="business_name_reason_change"]').val('');
                                $('input[name="phone_reason_change"]').val('');
                                $('input[name="affiliate_code_reason_change"]').val('');
                                $('input[name="requests_banks_reason_change"]').val('');
                                $('input[name="terminal_reason_change"]').val('');
                                $('input[name="num_terminal_reason_change"]').val('');
                                $('input[name="rif"]').val(res.rif_number);
                                $('input[name="userid"]').val(res.userid);
                                $('input[name="business_name"]').val(res.company);
                                $('input[name="requests_phone"]').val(res.phonenumber);
                                $('input[name="rif_bank_change"]').val(res.rif_number);
                                $('input[name="business_name_bank_change"]').val(res.company);
                                $('input[name="phone_bank_change"]').val(res.phonenumber);
                                console.log('1 ---valor del requests_banks_reason_change: ' + $("#requests_banks_change").val());

                            },
                            '5': () => { // desintalacion
                                console.log("caso 1 ")
                                $('input[name="rif_bank_change"]').val('');
                                $('input[name="business_name_bank_change"]').val('');
                                $('input[name="phone_bank_change"]').val('');
                                $('input[name="affiliate_code_bank_change"]').val('');
                                $('input[name="requests_banks_change"]').val('');
                                $('input[name="terminal_banks_change"]').val('');
                                $('input[name="num_terminal_banks_change"]').val('');
                                $('input[name="rif_reason_change"]').val('');
                                $('input[name="business_name_reason_change"]').val('');
                                $('input[name="phone_reason_change"]').val('');
                                $('input[name="affiliate_code_reason_change"]').val('');
                                $('input[name="requests_banks_reason_change"]').val('');
                                $('input[name="terminal_reason_change"]').val('');
                                $('input[name="num_terminal_reason_change"]').val('');
                                $('input[name="rif"]').val(res.rif_number);
                                $('input[name="userid"]').val(res.userid);
                                $('input[name="business_name"]').val(res.company);
                                $('input[name="requests_phone"]').val(res.phonenumber);
                            },
                            '6': () => { // correctivo
                                console.log("caso 6 ")
                                $('input[name="rif_bank_change"]').val('');
                                $('input[name="business_name_bank_change"]').val('');
                                $('input[name="phone_bank_change"]').val('');
                                $('input[name="affiliate_code_bank_change"]').val('');
                                $('input[name="requests_banks_change"]').val('');
                                $('input[name="terminal_banks_change"]').val('');
                                $('input[name="num_terminal_banks_change"]').val('');
                                $('input[name="rif_reason_change"]').val('');
                                $('input[name="business_name_reason_change"]').val('');
                                $('input[name="phone_reason_change"]').val('');
                                $('input[name="affiliate_code_reason_change"]').val('');
                                $('input[name="requests_banks_reason_change"]').val('');
                                $('input[name="terminal_reason_change"]').val('');
                                $('input[name="num_terminal_reason_change"]').val('');
                                $('input[name="rif"]').val(res.rif_number);
                                $('input[name="userid"]').val(res.userid);
                                $('input[name="business_name"]').val(res.company);
                                $('input[name="requests_phone"]').val(res.phonenumber);
                            },
                            '7': () => { // Camnio de simcard
                                console.log("caso 2 ")
                                $('input[name="rif_bank_change"]').val('');
                                $('input[name="business_name_bank_change"]').val('');
                                $('input[name="phone_bank_change"]').val('');
                                $('input[name="affiliate_code_bank_change"]').val('');
                                $('input[name="requests_banks_change"]').val('');
                                $('input[name="terminal_banks_change"]').val('');
                                $('input[name="num_terminal_banks_change"]').val('');
                                $('input[name="rif_reason_change"]').val('');
                                $('input[name="business_name_reason_change"]').val('');
                                $('input[name="phone_reason_change"]').val('');
                                $('input[name="affiliate_code_reason_change"]').val('');
                                $('input[name="requests_banks_reason_change"]').val('');
                                $('input[name="terminal_reason_change"]').val('');
                                $('input[name="num_terminal_reason_change"]').val('');
                                $('input[name="rif"]').val(res.rif_number);
                                $('input[name="userid"]').val(res.userid);
                                $('input[name="business_name"]').val(res.company);
                                $('input[name="requests_phone"]').val(res.phonenumber);
                            },

                        }
                        cas[type]();

                        AjaxTerminal(res.userid);

                    })
                }
            });

            function AjaxTerminal(id) {
                console.log('valor del id recibido desde function ajaxTerminals' + id);
                var operation = $('#type_operation').val();
                if (operation != '') {
                    console.log(operation);
                    var template = "<label for='terminal'>Serial</label>";
                    template += "<div class='input-group input-group-select select-terminal'>";
                    template += "<select name='terminal' id='terminal' class='ajax-search-terminal selectpicker jqTransformSelectWrapper' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php echo _l('dropdown_non_selected_tex'); ?>' >";
                    template += "<option value=''></option>";
                    template += "</select>";
                    template += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
                    template += "<a href='#' ><i class='fa fa-plus'></i></a>";
                    template += "</div>";
                    template += "</div>";

                    var terminal = {
                        '1': () => { // instalacion
                            $(".terminales").html(template);
                        },
                        '2': () => { //reactivacion
                            $(".terminales").html(template);
                        },
                        '3': () => { //cambio de razon social
                            $(".terminales").html(template);
                        },
                        '4': () => { // cambio de bancio
                            $(".terminales").html(template);
                        },
                        '5': () => { //desintalacion
                            $(".terminales").html(template);
                        },
                        '6': () => { //correctivo
                            //$(".terminales").html(template); 
                            var templates = "<label for='terminal'>Serial Saliente</label>";
                            templates += "<div class='input-group input-group-select select-terminal'>";
                            templates += "<select name='terminal_correctivo' id='terminal_correctivo' class='ajax-search-terminal selectpicker jqTransformSelectWrapperCorrectivo' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php echo _l('dropdown_non_selected_tex'); ?>' >";
                            templates += "<option value=''></option>";
                            templates += "</select>";
                            templates += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
                            templates += "<a href='#' ><i class='fa fa-plus'></i></a>";
                            templates += "</div>";
                            templates += "</div>";
                            $(".terminales_correctivo").html(templates);

                            init_ajax_search_terminal('terminal', '#terminal_correctivo.ajax-search-terminal', {
                                tickets_contacts: true,
                                contact_userid: id
                            });

                            $(".jqTransformSelectWrapperCorrectivo").on("click",
                                '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])',
                                function(event) {
                                    var t = $('#terminal_correctivo').val();
                                    $.post(admin_url + "tickets/ticket_change_data_terminal/", {
                                        contact_id: t
                                    }).done(
                                        function(e) {
                                            res = JSON.parse(e);
                                            if (res.contact_data) {
                                                console.log(res.contact_data);
                                                //SerializeArrayTerminalExists(res.contact_data);
                                            }

                                        }
                                    )
                                });
                        },
                        '7': () => { //cambio de simcard
                            //$(".terminales").html(template); 
                            var templates = "<label for='terminal'>Serial Saliente</label>";
                            templates += "<div class='input-group input-group-select select-terminal'>";
                            templates += "<select name='terminal_simcard' id='terminal_simcard' class='ajax-search-terminal selectpicker jqTransformSelectWrapperSimcard' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php echo _l('dropdown_non_selected_tex'); ?>' >";
                            templates += "<option value=''></option>";
                            templates += "</select>";
                            templates += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
                            templates += "<a href='#' ><i class='fa fa-plus'></i></a>";
                            templates += "</div>";
                            templates += "</div>";
                            $(".terminales_simcard").html(templates);

                            init_ajax_search_terminal('terminal', '#terminal_simcard.ajax-search-terminal', {
                                tickets_contacts: true,
                                contact_userid: id
                            });

                            $(".jqTransformSelectWrapperSimcard").on("click",
                                '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])',
                                function(event) {
                                    var t = $('#terminal_simcard').val();
                                    $.post(admin_url + "tickets/ticket_change_data_terminal/", {
                                        contact_id: t
                                    }).done(
                                        function(e) {
                                            res = JSON.parse(e);
                                            if (res.contact_data) {
                                                console.log(res.contact_data);
                                                //SerializeArrayTerminalExists(res.contact_data);
                                            }

                                        }
                                    )
                                });
                        },
                    }
                    terminal[operation]();
                    init_ajax_search_terminal('terminal', '#terminal.ajax-search-terminal', {
                        tickets_contacts: true,
                        contact_userid: id
                    });

                    $(".jqTransformSelectWrapper").on("click",
                        '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])',
                        function(event) {
                            var t = $('#terminal').val();
                            $.post(admin_url + "tickets/ticket_change_data_terminal/", {
                                contact_id: t
                            }).done(
                                function(e) {
                                    res = JSON.parse(e);
                                    if (res.contact_data) {
                                        console.log(res.contact_data);
                                        //SerializeArrayTerminalExists(res.contact_data);
                                    }

                                }
                            )
                        });

                }

                console.log('2---valor del requests_banks_reason_change: ' + $("#requests_banks_change").val());

            }

            $("#requests_banks_change").change(function() {
                console.log('cambio el banco ---valor del requests_banks_change: ' + $("#requests_banks_change").val());
            });



            $(".type_operation").change(function() {

                var type = $('#type_operation').val();
                console.log('type: ' + type);
                var cas = {
                    '1': () => { // instalacion
                        $('#requests_status_other').hide();
                        $('#requests_status_other').prop('disabled', true);
                        $('#requests_status').show();
                        $('#social_reason_change').hide();
                        $('#bank_change').hide();
                        $('#serial').hide();
                        $('#serial_install').show();
                        $('#correctivo').hide();
                        $('#serial_simcard').hide();
                    },
                    '2': () => { // reactivacion
                        $('#requests_status_other').hide();
                        $('#requests_status_other').prop('disabled', true);
                        $('#requests_status').show();
                        $('#social_reason_change').hide();
                        $('#bank_change').hide();
                        $('#serial').show();
                        $('#serial_install').hide()
                        $('#correctivo').hide();
                        $('#serial_simcard').hide();

                    },
                    '3': () => { //cambio de razon social
                        $('#requests_status_other').hide();
                        $('#requests_status_other').prop('disabled', true);
                        $('#requests_status').show();
                        $('#social_reason_change').show();
                        $('#serial').show();
                        $('#serial_install').hide()
                        $('#bank_change').hide();
                        $('#correctivo').hide();
                        $('#rif_bank_change').val('');
                        $('#business_name_bank_change').val('');
                        $('#phone_bank_change').val('');
                        $('#affiliate_code_bank_change').val('');
                        $('#requests_banks_change').val('');
                        $('#terminal_banks_change').val('');
                        $('#num_terminal_banks_change').val('');
                        $('#serial_simcard').hide();

                    },
                    '4': () => { //cambio de banco7
                        $('#requests_status_other').hide();
                        $('#requests_status_other').prop('disabled', true);
                        $('#requests_status').show();
                        $('#social_reason_change').hide();
                        $('#serial_install').hide();
                        $('#correctivo').hide();
                        $('#bank_change').show();
                        $('#serial').show();
                        $('#rif_reason_change').val('');
                        $('#business_name_reason_change').val('');
                        $('#phone_reason_change').val('');
                        $('#affiliate_code_reason_change').val('');
                        $('#requests_banks_reason_change').val('');
                        $('#terminal_reason_change').val('');
                        $('#num_terminal_reason_change').val('');
                        $('#serial_simcard').hide();


                    },
                    '5': () => { //desintalacion
                        $('#requests_status_other').show();
                        $('#requests_status').hide();
                        $('#requests_status').attr('disabled', true);
                        $('#social_reason_change').hide();
                        $('#bank_change').hide();
                        $('#serial_install').hide()
                        $('#serial').show();
                        $('#correctivo').hide();
                        $('#serial_simcard').hide();
                    },
                    '6': () => { // correctivo
                        $('#requests_status_other').hide();
                        $('#requests_status_other').prop('disabled', true);
                        $('#requests_status').show();
                        $('#social_reason_change').hide();
                        $('#bank_change').hide();
                        $('#serial_install').hide()
                        $('#serial').hide();
                        $('#correctivo').show();
                        $('#serial_simcard').hide();
                    },
                    '7': () => { // Cambio de simcard
                        $('#requests_status_other').hide();
                        $('#requests_status_other').prop('disabled', true);
                        $('#requests_status').show();
                        $('#social_reason_change').hide();
                        $('#bank_change').hide();
                        $('#serial_simcard').show();
                        $('#serial').hide();
                        $('#serial_install').hide()
                        $('#correctivo').hide();
                    },

                }
                cas[type]()
                if ($("#userid").val() != '')
                    AjaxTerminal($("#userid").val());

            });

            $("#rif_reason_change").on('change', function() {

                var formData = new FormData();

                var rif = $("#type_rif").val() + $("#rif_reason_change").val();
                console.log(rif);

                if (typeof(csrfData) !== 'undefined') {
                    formData.append(csrfData['token_name'], csrfData['hash']);
                }

                formData.append("rif", rif);

                $.ajax({
                    method: 'post',
                    data: formData,
                    contentType: false,
                    processData: false,
                    url: admin_url + "requests/rif_exists",
                    dataType: 'json'
                }).success(function(value) {
                    console.log(value);
                    if (value) {
                        $("#business_name_reason_change").val(value['company']);
                        $("#phone_reason_change").val(value['phonenumber']);
                    }
                    if (value == null) {
                        alert_float('danger', 'Cliente no existe.');
                        $("#business_name_reason_change").val('');
                        $("#phone_reason_change").val('');
                    }
                });

            });

            init_ajax_search_simcard('simcard_available', '#simcard.ajax-search', {
                tickets_contacts: true,
                contact_userid: function() {
                    // when ticket is directly linked to project only search project client id contacts
                    var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
                    if (uid) {
                        return uid;
                    } else {
                        return '';
                    }
                }
            }, );
            $(".simcard").click(function() {
                var t = $('#simcard').val();
                $.post(admin_url + "tickets/ticket_change_data_simcard", {
                    contact_id: t
                }).done(
                    function(e) {
                        res = JSON.parse(e).contact_data;
                        if (res) {
                            console.log(res.operadora);
                            $('select[id="operadoraid"]').selectpicker('val', res.operadora);
                        }
                    }
                )
            })




        });
    </script>