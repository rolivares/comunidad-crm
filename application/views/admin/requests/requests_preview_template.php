<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_hidden('_attachment_sale_id', $requests->id_operation); ?>
<?php echo form_hidden('_attachment_sale_type', 'proposal'); ?>
<div class="modal fade" id="modal_convert_document_sale" tabindex="-1" role="dialog" aria-hidden="true"  aria-labelledby="modal_convert_document_sale">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
               <button type="button" class="close " data-dismiss="modal" aria-label="Close" data-bs-dismiss="modal"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title">
                  <span class="add-title"><?php echo _l('proposal_convert_to_sales_document'); ?></span>
               </h4>
         </div>
         <div class="modal-body ">
            <div class="row mtop10">
               <div class="col-md-3">
                  <?php echo format_requests_status($requests->status, 'pull-left mright5 mtop5'); ?>
               </div>
               <div class="col-md-9 text-right _buttons proposal_buttons">
                  <?php if ($requests->invoice_id == NULL) { ?>
                     <?php if (has_permission('invoices', '', 'create')) { ?>
                        <div class="btn-group">
                           <button type="button" class="btn btn-success dropdown-toggle<?php if (total_rows(db_prefix() . 'clients', array('active' => 0, 'userid' => $requests->client_id)) > 0) {
                                                                                          echo ' disabled';
                                                                                       } ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?php echo _l('to_emit'); ?> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu dropdown-menu-right">
                              <?php
                              $disable_convert = false;
                              $not_related = false;
                              ?>

                              <?php if (has_permission('invoices', '', 'create')) { ?>
                                 <li <?php if ($disable_convert) {
                                          echo 'data-toggle="tooltip" title="' . _l($help_text, _l('requests_convert_sales_document')) . '"';
                                       } ?>>
                                    <a href="modal:close" <?php if ($disable_convert) {
                                                   echo 'style="cursor:not-allowed;" onclick="return false;"';
                                                } else {
                                                   echo 'data-template="invoice" onclick="requests_convert_template(this); return false;"';
                                                } ?>><?php echo _l('requests_convert_sales_document'); ?></a>
                                 </li>
                              <?php } ?>
                           </ul>
                        </div>
                     <?php } ?>
                  <?php } else {
                     if ($requests->invoice_id != NULL) {
                        echo '<a href="' . admin_url('invoices/list_invoices/' . $requests->invoice_id) . '" class="btn btn-info">' . format_invoice_number($requests->invoice_id) . '</a>';
                     }
                  } ?>
               </div>
            </div>
            <div class="clearfix"></div>
            <!-- <hr class="hr-panel-heading" /> -->
            <div class="row">
               <div class="col-md-12">
                  <div class="tab-content">
                     <div role="tabpanel" class="tab-pane active" id="tab_requests">
                        <div class="row mtop10">
                           <div class="col-md-6">
                              <h4 class="bold">
                                 <!-- <a href="<?php echo admin_url('requests/requests/' . $requests->id_operation); ?>"> -->
                                 <a>
                                    <span id="requests-number">
                                       <?php echo format_requests_number($requests->id_operation); ?>
                                    </span>
                                 </a>
                              </h4>
                              <h5 class="bold mbot15 font-medium">
                                 <!-- <a href="<?php echo admin_url('requests/requests/' . $requests->id_operation); ?>"> -->
                                 <a>
                                    <?php echo $requests->type_name; ?>
                                 </a>
                              </h5>
                              <address>
                                 <?php echo format_organization_info(); ?>
                              </address>
                           </div>
                           <div class="col-md-6 text-right">
                              <h4 class="bold">
                                 <address>
                                    <span class="bold"><?php echo _l('proposal_to'); ?>:</span><br />
                                    <a href="<?php echo admin_url('clients/client/' . $requests->client_id); ?>"><?php echo $requests->company; ?></a><br />
                                    <a href="<?php echo admin_url('clients/client/' . $requests->client_id); ?>"><?php echo $requests->address;  ?></a><br />
                                    <a href="<?php echo admin_url('clients/client/' . $requests->client_id); ?>"><?php echo $requests->rif_number; ?></a>

                                 </address>
                              </h4>
                           </div>
                        </div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<!-- </div> -->

<script>
   init_btn_with_tooltips();
   init_datepicker();
   init_selectpicker();
   init_form_reminder();
   init_tabs_scrollable();
   // defined in manage requests
   requests_id = '<?php echo $requests->id_operation; ?>';
   init_proposal_editor();
</script>