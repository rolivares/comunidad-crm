<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>

<div id="wrapper">
    <div class="content">
        <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'requests-form')) ;?>
        <div class="col-md">
            <div class="panel_s">
                <div class="panel-body">
                    <div>
                        <div id="divContent" class="tab-content">
                            <h4 class="customer-profile-group-heading"><?php echo _l('update_requests'); ?></h4>
                            <div class="col-md-12">
                                    <?php $request_id =isset($request['id_operation'])? $request['id_operation'] : '';?>
                                    <input type="hidden" name="request_id" id="request_id"  value="<?php echo $request_id ; ?>"/>   
                                    <input type="hidden" name="request_terminal_id" id="request_terminal_id"  value="<?php ($terminal_info != '') ? print $terminal_info['id']: print '' ; ?>"/>                
                                    <input type="hidden" name="request_terminal_serial" id="request_terminal_serial"  value="<?php ($terminal_info != '') ? print $terminal_info['serial']: print '' ; ?>"/>             
                                    
                                    <input type="hidden" name="hid_terminal_saliente" id="hid_terminal_saliente"  value="<?php ($terminal_saliente_info != '') ? print $terminal_saliente_info['id']: print '' ; ?>"/>  
                                    <input type="hidden" name="hid_terminal_saliente_serial" id="hid_terminal_saliente_serial"  value="<?php ($terminal_saliente_info != '') ? print $terminal_saliente_info['serial']: print '' ; ?>"/>  
                                    
                                    <input type="hidden" name="hid_terminal_entrante" id="hid_terminal_entrante"  value="<?php ($terminal_entrante_info != '') ? print $terminal_entrante_info['id']: print '' ; ?>"/>  
                                    <input type="hidden" name="hid_terminal_entrante_serial" id="hid_terminal_entrante_serial"  value="<?php ($terminal_entrante_info != '') ? print $terminal_entrante_info['serial']: print '' ; ?>"/>  

                                    <input type="hidden" name="my_type_operation" id="my_type_operation"  value="<?php ($request['type'] != '') ? print $request['type']: print '' ; ?>"/>  
 
                            
                                    <div class="col-md-6">
                                        <label class="control-label"><?php echo _l('type_requests'); ?></label>
                                        <div class="form-group select-placeholder" id="type_requests">
                                            <select name="type_operation" id="type_operation" required="true" data-width="100%" disabled="true"
                                                data-live-search="true" class="selectpicker model type_operation"
                                                data-title="<?php echo _l('Nada seleccionado') ; ?>">
                                                <?php $my_type = isset($request['type']) ? $request['type'] : '';?>
                                                <?php foreach ($type_operation as $type_operation) { ?>
                                                <option value="<?php echo $type_operation['id_type']; ?>"<?php if ($my_type == $type_operation['id_type']) echo 'selected'; ?>>
                                                    <?php echo $type_operation['name'];  ?></option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="contactid"><?php echo _l('contact'); ?></label>
                                        <div class="form-group select-placeholder" id="ticket_contact_w">
                                            <select name="contactid" required="true" id="contactid"
                                                class="ajax-search search-contactid" data-width="100%"
                                                data-abs-cache="false" data-live-search="true"
                                                data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                <?php if(isset($contact)) { ?>
                                                <option value="<?php echo $contact['id']; ?>" selected>
                                                    <?php echo $contact['firstname'] . ' ' .$contact['lastname']; ?>
                                                </option>
                                                <?php } ?>
                                                <option value=""></option>
                                            </select>
                                            <?php echo render_input('userid','userid','','hidden'); ?>
                                        </div>
                                    </div>
                            </div> 
                            <div class="col-md-12">
                                    <div class="col-md-6">
                                            <label for="chanel"><?php echo _l('requests_chanel'); ?></label>
                                            <?php echo render_select_terminal('chanel','chanel',$chanels,array('id','name'),_l('chanel'),(isset($request['chanel']) ? $request['chanel'] : '')); ?>
                                    </div>
                            </div>
                                
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label for="requests_status"><?php echo _l('requests_status'); ?></label>
                                    <?php echo render_select_terminal('requests_status',_l('requests_status'),$status,array('id_status','name'),_l('requests_status'),(isset($request['status']) ? $request['status'] : '1'),array('required'=>true)); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo render_date_input('date','requests_date_change_status',(isset($request['status_date']) ? $request['status_date'] : date('Y-m-d')),array('required'=>true)); ?>
                                </div>
                            </div>
                              
                            <div class="col-md-12">
                                <div id='form'>
                                    <div class="col-md-3">
                                        <?php echo render_input('rif','requests_rif','','text',array('disabled' => 'true')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php echo render_input('business_name','type_requests_razon_social','','text', array('disabled' => 'true')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php echo render_input('requests_phone', 'requests_phone','','number', array('disabled' => 'true')); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php echo render_input('requests_affiliate_code','requests_affiliate_code',(isset($request['affiliate_code']) ? $request['affiliate_code'] : ''),'number'); ?>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label for="requests_banks"><?php echo _l('requests_banks'); ?></label>
                                        <?php echo render_select_terminal('requests_banks','requests_banks',$banks,array('id','banks'),_l('requests_banks'),(isset($request['bank_id']) ? $request['bank_id'] : '')); ?>
                                    </div>
                                    <div class="col-md-3" id="serial" style='display: none'>
                                        <div
                                            class="terminales form-group form-group-select-input-service input-group-select">
                                            <label for="terminal"><?php echo _l('requests_serial'); ?></label>
                                            <div class='input-group input-group-select select-terminal'>
                                                <select name="terminal" id="terminal"
                                                    class="ajax-search-terminal selectpicker jqTransformSelectWrapper"
                                                    data-width="100%" data-live-search="true" data-abs-cache="false"
                                                    data-abs-bind-event="click" data-abs-preserve-selected="false"
                                                    data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"
                                                    disabled>
                                                    <option value=""></option>
                                                </select>
                                                <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                    <a href="#"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---- campos para cambio de simcard ---------->
                                    <div  id="serial_simcard" style='display: none'>
                                        <div class="col-md-3">
                                            <div class="terminales_simcard form-group form-group-select-input-service input-group-select">
                                                <label for="terminal"><?php echo _l('requests_serial'); ?></label>
                                                <div class='input-group input-group-select select-terminal'>
                                                    <select name="terminal_simcard" id="terminal_simcard"
                                                        class="ajax-search-terminal selectpicker jqTransformSelectWrapperSimcard"
                                                        data-width="100%" data-live-search="true" data-abs-cache="false"
                                                        data-abs-bind-event="click" data-abs-preserve-selected="false"
                                                        data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"
                                                        disabled>
                                                        <option value=""></option>
                                                    </select>
                                                    <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                        <a href="#"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                       
                                        <!--div class="col-md-3">
                                            <div class="form-group" app-field-wrapper="simcard">
                                                        <label for="simcard" class="control-label"><?php// echo _l('sim_card_serial'); ?></label>
                                                        <select name="simcard" required="true" id="simcard" 
                                                                class="ajax-search simcard" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                            <option value=""></option>
                                                        </select>
                                                        <p id="serialsimcard-error" class="text-danger" style="display: none;"> </p>
                                            </div>
                                        </div-->
                                        <div class="col-md-3">
                                            <label class="control-label"><?php echo _l('sim_card_serial'); ?></label>
                                            <div class="form-group select-placeholder" id="group_simcard">
                                                <select name="simcard" id="simcard" required="true" data-width="100%" 
                                                data-live-search="true" class="selectpicker model  ajax-search simcard"
                                                data-title="<?php echo _l('Nada seleccionado') ; ?>">
                                                <?php if(!empty($mysimcard)){  ?>                                                      
                                                    <option value="<?php echo $mysimcard['id']; ?>"<?php  echo 'selected'; ?>>
                                                    <?php echo $mysimcard['serialsimcard'];  ?></option>                                                 
                                                <?php }          
                                                foreach ($simcards as $sim) { ?>
                                                <option value="<?php echo $sim['id']; ?>">
                                                    <?php echo $sim['serialsimcard'];  ?></option>
                                                <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                     <!---- campos para cambio de simcard ---------->
                                    <div class="col-md-3" id="serial_install" style='display: none'>
                                        <div id="div_serial_install"
                                            class="terminales form-group form-group-select-input-service input-group-select">
                                            <label for="terminal"><?php echo _l('requests_serial'); ?></label>
                                            <div class='input-group input-group-select select-terminal'>
                                                <select name="requests_serial_install" id="requests_serial_install"
                                                    class="ajax-search-terminal selectpicker jqTransformSelectWrapper"
                                                    data-width="100%" data-live-search="true" data-abs-cache="false"
                                                    data-abs-bind-event="click" data-abs-preserve-selected="false"
                                                    data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"
                                                    disabled>
                                                    <option value=""></option>
                                                </select>
                                                <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                    <a href="#"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--div class="col-md-3" id="serial_install" style='display: none'>
                                        <div class="terminales" >
                                            <div id="additional"><?php // echo _l('requests_serial'); ?></div>
                                            <?php // echo render_select_terminal('requests_serial_install', 'requests_serial',$lisTerminals,array('id','serial'),'Banco',(isset($request['terminal']) ? $request['terminal'] : '')); ?>
                                        </div> 
                                    </div-->
                                  
                                    <div id="correctivo" style='display: none'>
                                        <div class="col-md-3">
                                            <div class="terminales_correctivo form-group form-group-select-input-service input-group-select">
                                                <label for="terminal"><?php echo _l('requests_serial_outgoing'); ?></label>
                                                <div class='input-group input-group-select select-terminal'>
                                                    <select name="terminal_correctivo" id="terminal_correctivo"
                                                        class="ajax-search-terminal selectpicker jqTransformSelectWrapperCorrectivo"
                                                        data-width="100%" data-live-search="true" data-abs-cache="false"
                                                        data-abs-bind-event="click" data-abs-preserve-selected="false"
                                                        data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"
                                                        disabled>
                                                        <option value=""></option>
                                                    </select>
                                                    <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                        <a href="#"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                          <label class="control-label"><?php echo _l('requests_serial_incoming'); ?></label>
                                          <div class="form-group select-placeholder" id="requests_serial_incoming_div">
                                              <select name="requests_serial_incoming" id="requests_serial_incoming" required="true" data-width="100%"
                                                  data-live-search="true" class="selectpicker model requests_serial_incoming"
                                                  data-title="<?php echo _l('Nada seleccionado') ; ?>">
                                                  
                                                    <?php if ($terminal_entrante_info!=''){?>
                                                       <option value="<?php echo $terminal_entrante_info['id']; ?>" selected >
                                                        <?php echo $terminal_entrante_info['serial']; ?>
                                                        </option>
                                                    <?php } ?>

                                                  <?php foreach ($lisTerminals as $term) { ?>
                                                    <option value="<?php echo $term['id']; ?>">
                                                      <?php echo $term['serial'];  ?></option>
                                                      <?php } ?>
                                              </select>
                                          </div>
                                        </div>
                                    </div>    

                                    <div class="col-md-3">
                                        <?php echo render_input('num_terminal','requests_terminals',(isset($request['num_terminal']) ? $request['num_terminal'] : ''),'number'); ?>
                                    </div>

                                    
                                    <div id='social_reason_change' style='display: none'>
                                       
                                        <div class="col-md-12">
                                            <h5><?php echo _l('type_requests_new_razon_social'); ?></h5>
                                        </div>
                                        <div class="col-md-1">
                                            <?php echo render_select('type_rif', $type_rif,array('type_rif','type_rif'),'type_rif',(isset($request['reason_change_type_rif']) ? $request['reason_change_type_rif'] : ''));?>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo render_input('rif_reason_change', 'requests_rif',(isset($request['reason_change_num_rif']) ? $request['reason_change_num_rif'] : ''),'text'); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('business_name_reason_change', 'type_requests_razon_social',(isset($request['business_name_reason_change']) ? $request['business_name_reason_change'] : ''),'text'); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('phone_reason_change','requests_phone',(isset($request['phone_reason_change']) ? $request['phone_reason_change'] : ''),'text'); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('affiliate_code_reason_change','requests_affiliate_code',(isset($request['affiliate_code_reason_change']) ? $request['affiliate_code_reason_change'] : ''),'number'); ?>
                                        </div>
                                       
                                        
                                        <div class="col-md-3">
                                            <label for="requests_banks"><?php echo _l('requests_banks'); ?></label>
                                            <?php echo render_select_terminal('requests_banks_reason_change','requests_banks_reason_change',$banks,array('id','banks'),_l('requests_banks'),(isset($request['requests_banks_reason_change']) ?  $request['requests_banks_reason_change'] : '')); ?>
                                        </div>
                                        

                                        <input type="hidden" name="hid_banks_reason_change" id="hid_banks_reason_change"  value="<?php ($request['requests_banks_reason_change'] != '') ? print $request['requests_banks_reason_change']: print 'n/a' ; ?>"/>             


                                        <!--div class="col-md-3">
                                            <div
                                                class="terminales_reason_change form-group form-group-select-input-service input-group-select">
                                                <label for="terminal"><?php //echo _l('requests_serial'); ?></label>
                                                <div class='input-group input-group-select select-terminal'>
                                                    <select name="terminal_reason_change" id="terminal_reason_change"
                                                        class="ajax-search-terminal selectpicker jqTransformSelectWrapperReasonChange"
                                                        data-width="100%" data-live-search="true" data-abs-cache="false"
                                                        data-abs-bind-event="click" data-abs-preserve-selected="false"
                                                        data-none-selected-text="<?php //echo _l('dropdown_non_selected_tex'); ?>"
                                                        disabled>
                                                        <option value=""></option>
                                                    </select>
                                                    <div id='addterminal' class='input-group-addon' style="opacity: 1;">
                                                        <a href="#"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div-->
                                        <div class="col-md-3">
                                            <?php echo render_input('num_terminal_reason_change','requests_terminals',(isset($request['num_terminal_reason_change']) ? $request['num_terminal_reason_change'] : ''),'number'); ?>
                                        </div>
                                       
                                    </div>
                                    <div id='bank_change'  style='display: none'>
                                        <div class="col-md-12">
                                            <h5><?php echo _l('type_requests_new_cambio_banco'); ?></h5>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('rif_bank_change', 'requests_rif',(isset($request['rif_bank_change']) ? $request['rif_bank_change'] : ''),'text',array('disabled' => 'true')); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('business_name_bank_change', 'type_requests_razon_social',(isset($request['business_name_bank_change']) ? $request['business_name_bank_change'] : ''),'text',array('disabled' => 'true')); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('phone_bank_change','requests_phone',(isset($request['phone_bank_change']) ? $request['phone_bank_change'] : ''),'text',array('disabled' => 'true')); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo render_input('affiliate_code_bank_change','requests_affiliate_code',(isset($request['affiliate_code_bank_change']) ? $request['affiliate_code_bank_change'] : ''),'number'); ?>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <label for="requests_banks"><?php echo _l('requests_banks'); ?></label>
                                            <?php echo render_select_terminal('requests_banks_change','requests_banks',$banks,array('id','banks'),_l('requests_banks'),(isset($request['requests_banks_change']) ? $request['requests_banks_change'] : '')); ?>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <?php echo render_input('num_terminal_banks_change','requests_terminals',(isset($request['num_terminal_banks_change']) ? $request['num_terminal_banks_change'] : ''),'number'); ?>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-12">                                        
                                        <?php echo render_textarea('observation','requests_observation',(isset($request['observation']) ? $request['observation'] : '')); ?>
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info" id="submit"><?php echo _l('submit'); ?></button>
                </div>
            </div>

        </div>
        <?php echo form_close(); ?>
        <?php init_tail(); ?>

    </div>
    <script>
    $(function() {
        var request_id = $('#request_id').val();         
        var st=  $('#requests_status').val();
        var my_type_operation = $('#my_type_operation').val();
            
            console.log('1- dentro d la funcion js cargada al iniciar -- valor del requests_status: '+st);

        if (request_id!=''){ 
            console.log('2- se llama a  changue imputs  y changue_data -- valor del requests_id: '+request_id);   
            chage_imputs();
            change_data();
        }

       /* appValidateForm($('#requests-form'), {
            name: 'required',
            type_operation: 'required',
            contactid: 'required',
            phone_reason_change: {
                maxlength:11
            }
        }, save_requests); */

        $('#requests-form').validate({
            rules: {
                name: 'required',
                type_operation: 'required',
                contactid: 'required',
                phone_reason_change: {
                    maxlength:11
                }
            },
            submitHandler: function save_requests() {
                //var formData = new FormData($('#requests-form')[0]);

                var formData = new FormData();
                let t = $('select[id="terminal"]').val()
                console.log($('select[id="terminal"]').val()) ;   
                if (typeof(csrfData) !== 'undefined') {
                    formData.append(csrfData['token_name'], csrfData['hash']);
                }
                console.log(serial_install); 

                formData.append("terminal", $('select[id="terminal"]').val());
                formData.append("userid", $("#userid").val());
                //formData.append("type_operation", $("#type_operation").val());
                formData.append("type_operation", $("#my_type_operation").val());

                formData.append("contactid", $("#contactid").val());
                formData.append("requests_status", $("#requests_status").val());
                formData.append("date", $("#date").val());
                formData.append("rif", $("#rif").val());
                formData.append("business_name",  $("#business_name").val());
                formData.append("requests_phone",  $("#requests_phone").val());
                formData.append("requests_affiliate_code",  $("#requests_affiliate_code").val());
                formData.append("requests_banks",  $("#requests_banks").val());

                formData.append("num_terminal",  $("#num_terminal").val());
                formData.append("observation",  $("#observation").val());
                formData.append("chanel",  $("#chanel").val());
                /* campos div de instalacion */
                formData.append("requests_serial_install", $("#requests_serial_install").val());
                /* campos div de correcion */
                formData.append("terminal_saliente", $("#terminal_correctivo").val());
                formData.append("terminal_entrante", $("#requests_serial_incoming").val());
                /* campos div de nueva razon social */
                formData.append("rif_reason_change", $("#rif_reason_change").val());
                formData.append("business_name_reason_change", $("#business_name_reason_change").val());
                formData.append("phone_reason_change", $("#phone_reason_change").val());
                formData.append("affiliate_code_reason_change", $("#affiliate_code_reason_change").val());
                formData.append("requests_banks_reason_change", $("#requests_banks_reason_change").val());
                formData.append("terminal_reason_change", $("#terminal_reason_change").val());
                formData.append("num_terminal_reason_change", $("#num_terminal_reason_change").val());
                /* campos div  cambio SIMCARD */
                formData.append("simcard", $("#simcard").val());                
                formData.append("terminal_simcard", $("#terminal_simcard").val());
                /* campos div de nueva cambio de banco */
                formData.append("rif_bank_change", $("#rif_bank_change").val());
                formData.append("business_name_bank_change", $("#business_name_bank_change").val());
                formData.append("phone_bank_change", $("#phone_bank_change").val());
                formData.append("affiliate_code_bank_change", $("#affiliate_code_bank_change").val());
                formData.append("requests_banks_change", $("#requests_banks_change").val());
                formData.append("terminal_banks_change", $("#terminal_banks_change").val());
                formData.append("num_terminal_banks_change", $("#num_terminal_banks_change").val());
                formData.append("type_rif", $("#type_rif").val());            
                formData.append("request_id",$('#request_id').val());    

                $("#submit").prop("disabled", true);

                $.ajax({
                    method: 'post',
                    data: formData,
                    contentType: false,
                    processData: false,
                    url: admin_url + 'requests/requests/'+ $('#request_id').val(),
                    dataType: 'json'

                }).done(function(response) {
                    //console.log(response);    
                    if (response.status) {
                        //console.log(response.status);
                        window.location.href = admin_url + 'requests';
                       // alert_float('success', response.message);
                    }
                }).fail(function(error) {
                    alert_float('danger', JSON.parse(error.responseText));
                    //console.log("error");
                });

            }
        })
        


        var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
        init_ajax_search('contact', '#contactid.ajax-search', {
            tickets_contacts: true,
            contact_userid: function() {
                var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
                if (uid) {
                    return uid;
                } else {
                    return '';
                }
            }
        },);

        $(".ajax-search .dropdown-menu").click(function() {
             change_data();
        });

               // funcion que blaquea y oculta o muestra los campos segun el tipo de operacion
        function chage_imputs() {
            console.log('activando y ocultando campos desde la funcion change_imputs');

            var type = $('#type_operation').val();

            console.log(type);
            var cas = { 
                        '1': ()=>{ // instalacion
                            $('#social_reason_change').hide();
                            $('#bank_change').hide();
                            $('#serial').hide();
                            //$('#serial_install').show();
                            //$('#correctivo').hide();
                            $('#serial').show();
                            $('#serial_install').hide();
                        },
                        '2': ()=>{ // reactivacion
                            $('#social_reason_change').hide();
                            $('#bank_change').hide();
                            $('#serial').show();
                            $('#serial_install').hide()
                            $('#correctivo').hide();
                        },
                        '3': ()=>{ //cambio de razon social
                            $('#social_reason_change').show();
                            $('#serial').show();
                            $('#serial_install').hide()
                            $('#bank_change').hide();
                            $('#correctivo').hide();
                            $('#rif_bank_change').val('');
                            $('#business_name_bank_change').val('');
                            $('#phone_bank_change').val('');
                            $('#affiliate_code_bank_change').val('');
                            $('#requests_banks_change').val('');
                            $('#terminal_banks_change').val('');
                            $('#num_terminal_banks_change').val(''); 
                            

                        },
                        '4': ()=>{ //cambio de banco
                            $('#social_reason_change').hide();
                            $('#serial_install').hide();
                            $('#correctivo').hide();
                            $('#bank_change').show();
                            $('#serial').show();
                            $('#rif_reason_change').val('');
                            $('#business_name_reason_change').val('');
                            $('#phone_reason_change').val('');
                            $('#affiliate_code_reason_change').val('');
                            $('#requests_banks_reason_change').val('');
                            $('#terminal_reason_change').val('');
                            $('#num_terminal_reason_change').val('');
                        },
                        '5': ()=>{ //desintalacion
                            $('#social_reason_change').hide();
                            $('#bank_change').hide();
                            $('#serial_install').hide()
                            $('#serial').show();
                            $('#correctivo').hide();
                        },
                        '6': ()=>{ // correctivo
                            $('#social_reason_change').hide();
                            $('#bank_change').hide();
                            $('#serial_install').hide()
                            $('#serial').hide();
                            $('#correctivo').show();
                        },
                        '7': ()=>{ // Cambio de simcard
                            $('#social_reason_change').hide();
                            $('#bank_change').hide();
                            $('#serial_simcard').show();
                            $('#serial').hide();
                            $('#serial_install').hide()
                            $('#correctivo').hide();
                        },
                }
            cas[my_type_operation]();
            
 
        }

        // funcion para asignar los datos del contacto
        // y vaciar otros campos segun el tipo de operacion
        function change_data(){
                
            var e;
            var t = $('select[name="contactid"]').val();
            var a = $('select[name="project_id"]');
            var i = a.attr("data-auto-project");
            var n = $(".projects-wrapper");
            var stock_name = [];
           
 

            if (t != '') {
                
 
                $.post(admin_url + "requests/requests_change_data/", {
                    contact_id: t
                }).done(function(e) {
                    var res = JSON.parse(e).contact_data;
                    //var type = $('#type_operation').val();
                    var cas = { 
                        '1': ()=>{ //activacion
                            console.log("caso 1 ")
                            $('input[name="rif_bank_change"]').val('');
                            $('input[name="business_name_bank_change"]').val('');
                            $('input[name="phone_bank_change"]').val('');
                            $('input[name="affiliate_code_bank_change"]').val('');
                            $('input[name="requests_banks_change"]').val('');
                            $('input[name="terminal_banks_change"]').val('');
                            $('input[name="num_terminal_banks_change"]').val('');
                            $('input[name="rif_reason_change"]').val('');
                            $('input[name="business_name_reason_change"]').val('');
                            $('input[name="phone_reason_change"]').val('');
                            $('input[name="affiliate_code_reason_change"]').val('');
                            $('input[name="requests_banks_reason_change"]').val('');
                            $('input[name="terminal_reason_change"]').val('');
                            $('input[name="num_terminal_reason_change"]').val('');
                            $('input[name="rif"]').val(res.rif_number);
                            $('input[name="userid"]').val(res.userid);
                            $('input[name="business_name"]').val(res.company);
                            $('input[name="requests_phone"]').val(res.phonenumber);
                            $('input[name="terminal"]').val('');
                        }, 
                        '2': ()=>{ // reactivacion
                            console.log("caso 2 ")
                            $('input[name="rif_bank_change"]').val('');
                            $('input[name="business_name_bank_change"]').val('');
                            $('input[name="phone_bank_change"]').val('');
                            $('input[name="affiliate_code_bank_change"]').val('');
                            $('input[name="requests_banks_change"]').val('');
                            $('input[name="terminal_banks_change"]').val('');
                            $('input[name="num_terminal_banks_change"]').val('');
                            $('input[name="rif_reason_change"]').val('');
                            $('input[name="business_name_reason_change"]').val('');
                            $('input[name="phone_reason_change"]').val('');
                            $('input[name="affiliate_code_reason_change"]').val('');
                            $('input[name="requests_banks_reason_change"]').val('');
                            $('input[name="terminal_reason_change"]').val('');
                            $('input[name="num_terminal_reason_change"]').val('');
                            $('input[name="rif"]').val(res.rif_number);
                            $('input[name="userid"]').val(res.userid);
                            $('input[name="business_name"]').val(res.company);
                            $('input[name="requests_phone"]').val(res.phonenumber);
                        },  
                        '3': ()=>{ // cambio de razon social
                            console.log("caso 3")
                            $('input[name="rif_bank_change"]').val('');
                            $('input[name="business_name_bank_change"]').val('');
                            $('input[name="phone_bank_change"]').val('');
                            $('input[name="affiliate_code_bank_change"]').val('');
                            $('input[name="requests_banks_change"]').val('');
                            $('input[name="terminal_banks_change"]').val('');
                            $('input[name="num_terminal_banks_change"]').val('');
                            $('input[name="rif"]').val(res.rif_number);
                            $('input[name="userid"]').val(res.userid);
                            $('input[name="business_name"]').val(res.company);
                            $('input[name="requests_phone"]').val(res.phonenumber);
                            //$('input[name="requests_banks_reason_change"]').val($("#hid_banks_reason_change").val());    
                            //-------------------prueba  inicializacion de los sectpickers
                            //console.log('inicializando select piker requests_banks_reason_change');
                           // $('select[id="requests_banks_reason_change"]').selectpicker();
                            //$('select[id="requests_banks_reason_change"]').selectpicker('val','2');  
                            //$('select[id="requests_banks_reason_change"]').selectpicker('val', ['Johk', 'Paul']);
                            //$('.selectpicker').selectpicker('refresh');                     


                            console.log('Dentro del caso 3 de changue data: '+ $("#hid_banks_reason_change").val());
                            console.log('valor del HIDDEN hid_banks_reason_change: '+ $("#hid_banks_reason_change").val());

                            //$('input[name="requests_banks_reason_change"]').val($("#hid_banks_reason_change").val());                             
                            
                        },
                        '4': ()=>{ //cambio de banco
                            console.log("caso 4")
                            $('input[name="rif_reason_change"]').val('');
                            $('input[name="business_name_reason_change"]').val('');
                            $('input[name="phone_reason_change"]').val('');
                            $('input[name="affiliate_code_reason_change"]').val('');
                            $('input[name="requests_banks_reason_change"]').val('');
                            $('input[name="terminal_reason_change"]').val('');
                            $('input[name="num_terminal_reason_change"]').val('');
                            $('input[name="rif"]').val(res.rif_number);
                            $('input[name="userid"]').val(res.userid);
                            $('input[name="business_name"]').val(res.company);
                            $('input[name="requests_phone"]').val(res.phonenumber);
                            $('input[name="rif_bank_change"]').val(res.rif_number);
                            $('input[name="business_name_bank_change"]').val(res.company);
                            $('input[name="phone_bank_change"]').val(res.phonenumber);
                            console.log('valor del requests_banks_reason_change: '+ $("#requests_banks_change").val());

                        },
                        '5': ()=>{ // desintalacion
                            console.log("caso 5 ")
                            $('input[name="rif_bank_change"]').val('');
                            $('input[name="business_name_bank_change"]').val('');
                            $('input[name="phone_bank_change"]').val('');
                            $('input[name="affiliate_code_bank_change"]').val('');
                            $('input[name="requests_banks_change"]').val('');
                            $('input[name="terminal_banks_change"]').val('');
                            $('input[name="num_terminal_banks_change"]').val('');
                            $('input[name="rif_reason_change"]').val('');
                            $('input[name="business_name_reason_change"]').val('');
                            $('input[name="phone_reason_change"]').val('');
                            $('input[name="affiliate_code_reason_change"]').val('');
                            $('input[name="requests_banks_reason_change"]').val('');
                            $('input[name="terminal_reason_change"]').val('');
                            $('input[name="num_terminal_reason_change"]').val('');
                            $('input[name="rif"]').val(res.rif_number);
                            $('input[name="userid"]').val(res.userid);
                            $('input[name="business_name"]').val(res.company);
                            $('input[name="requests_phone"]').val(res.phonenumber);
                        },
                        '6': ()=>{ // correctivo
                            console.log("caso 6 ")
                            $('input[name="rif_bank_change"]').val('');
                            $('input[name="business_name_bank_change"]').val('');
                            $('input[name="phone_bank_change"]').val('');
                            $('input[name="affiliate_code_bank_change"]').val('');
                            $('input[name="requests_banks_change"]').val('');
                            $('input[name="terminal_banks_change"]').val('');
                            $('input[name="num_terminal_banks_change"]').val('');
                            $('input[name="rif_reason_change"]').val('');
                            $('input[name="business_name_reason_change"]').val('');
                            $('input[name="phone_reason_change"]').val('');
                            $('input[name="affiliate_code_reason_change"]').val('');
                            $('input[name="requests_banks_reason_change"]').val('');
                            $('input[name="terminal_reason_change"]').val('');
                            $('input[name="num_terminal_reason_change"]').val('');
                            $('input[name="rif"]').val(res.rif_number);
                            $('input[name="userid"]').val(res.userid);
                            $('input[name="business_name"]').val(res.company);
                            $('input[name="requests_phone"]').val(res.phonenumber);
                        },
                        '7': ()=>{ // Camnio de simcard
                            console.log("caso 7 ")
                            $('input[name="rif_bank_change"]').val('');
                            $('input[name="business_name_bank_change"]').val('');
                            $('input[name="phone_bank_change"]').val('');
                            $('input[name="affiliate_code_bank_change"]').val('');
                            $('input[name="requests_banks_change"]').val('');
                            $('input[name="terminal_banks_change"]').val('');
                            $('input[name="num_terminal_banks_change"]').val('');
                            $('input[name="rif_reason_change"]').val('');
                            $('input[name="business_name_reason_change"]').val('');
                            $('input[name="phone_reason_change"]').val('');
                            $('input[name="affiliate_code_reason_change"]').val('');
                            $('input[name="requests_banks_reason_change"]').val('');
                            $('input[name="terminal_reason_change"]').val('');
                            $('input[name="num_terminal_reason_change"]').val('');
                            $('input[name="rif"]').val(res.rif_number);
                            $('input[name="userid"]').val(res.userid);
                            $('input[name="business_name"]').val(res.company);
                            $('input[name="requests_phone"]').val(res.phonenumber);

                        },
                    }
                    cas[my_type_operation]();
                    AjaxTerminal(res.userid);                    
                })
            }
        }

        function AjaxTerminal(id) {

           console.log('dentp de la funcion ajaxTerminal - res.userid:'+id);
            var operation = $('#type_operation').val();
            //opciones usadas por defecto para cargar la data del selector de terminales
            var type_search= 'current_and_own_terminals';                        
            var target_select='#terminal.ajax-search-terminal';

            var request_terminal_id= $('#request_terminal_id').val();             console.log('request_terminal_id: '+request_terminal_id);
            var request_terminal_serial= $('#request_terminal_serial').val();     console.log('request_terminal_serial: '+request_terminal_serial);

            var terminal_saliente = $('#hid_terminal_saliente').val();           console.log('terminal_saliente : '+terminal_saliente);
            var terminal_saliente_serial = $('#hid_terminal_saliente_serial').val();    console.log('terminal_saliente : '+terminal_saliente);            
            
            var terminal_entrante = $('#hid_terminal_entrante').val();           console.log('terminal_entrante: '+terminal_entrante);
            var terminal_entrante_serial= $('#hid_terminal_entrante_serial').val();    console.log('terminal_entrante: '+terminal_entrante);           

                
            console.log(operation);

            
                // var template = "<label for='terminal'>Serial</label>";
                //     template += "<div class='input-group input-group-select select-terminal'>";
                //     template += "<select name='terminal' id='terminal' class='ajax-search-terminal selectpicker jqTransformSelectWrapper' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php // echo _l('dropdown_non_selected_tex'); ?>' >";
                //     template += "<option value=''></option>";
                //     template += "</select>";
                //     template += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
                //     template += "<a href='#' ><i class='fa fa-plus'></i></a>";
                //     template += "</div>";
                //     template += "</div>";
            var template = "<label for='terminal'>Serial</label>";
                template += "<div class='input-group input-group-select select-terminal'>";
                template += "<select name='terminal del template' id='terminal' class='ajax-search-terminal selectpicker jqTransformSelectWrapper' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='nada seleccionado' > <?php if( $terminal_info != '') { ?>
                             <option value='"+request_terminal_id+"' selected>";
                template += request_terminal_serial+"</option>  <?php } ?>";
                template += "<option value=''></option>";
                template += "</select>";
                template += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
                template += "<a href='#' ><i class='fa fa-plus'></i></a>";
                template += "</div>";
                template += "</div>";
                   


            var terminal = { 
                '1': ()=>{ // instalacion
                    $(".terminales").html(template); 
                    type_search= 'current_and_availables_terminals';
                    /*init_ajax_search_2(type_search,'#requests_serial_install.ajax-search-terminal', {
                        tickets_contacts: true,
                        contact_userid: id
                        //term_id: request_terminal_id
                    },request_terminal_id);*/
                },
                '2': ()=>{ //reactivacion
                    $(".terminales").html(template); 
                    type_search= 'current_and_uninstall_terminals';
                },
                '3': ()=>{ //cambio de razon social
                    $(".terminales").html(template); 
                },
                '4': ()=>{ // cambio de bancio
                    $(".terminales").html(template);    
                },
                '5': ()=>{ //desintalacion
                    $(".terminales").html(template); 
                },
                '6': ()=>{ //correctivo
                    //$(".terminales").html(template); 
                    var templates = "<label for='terminal'>Serial Saliente</label>";
                    templates += "<div class='input-group input-group-select select-terminal'>";
                    templates += "<select name='terminal_correctivo' id='terminal_correctivo' class='ajax-search-terminal selectpicker jqTransformSelectWrapperCorrectivo' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php echo _l('dropdown_non_selected_tex'); ?>' > ";
                    if(terminal_saliente!=''){
                        templates += "<option value='"+terminal_saliente+"' selected>"+terminal_saliente_serial+"</option>";
                    }
                    templates += "<option value=''></option>";
                    templates += "</select>";
                    templates += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
                    templates += "<a href='#' ><i class='fa fa-plus'></i></a>";
                    templates += "</div>";
                    templates += "</div>";
                    $(".terminales_correctivo").html(templates);

                    init_ajax_search_terminal('terminal', '#terminal_correctivo.ajax-search-terminal', {
                        tickets_contacts: true,
                        contact_userid: id
                    });

                    $(".jqTransformSelectWrapperCorrectivo").on("click",
                        '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])',
                        function(event) {
                            var t = $('#terminal_correctivo').val();
                            $.post(admin_url + "tickets/ticket_change_data_terminal/", {
                                contact_id: t
                            }).done(
                                function(e) {
                                    res = JSON.parse(e);
                                    if (res.contact_data) {
                                        console.log(res.contact_data);
                                        //SerializeArrayTerminalExists(res.contact_data);
                                    }
                                
                                }
                            )
                        });    
                },
                '7': ()=>{ //cambio de simcard
                   //$(".terminales").html(template); 
                   var templates = "<label for='terminal'>Serial</label>";
                    templates += "<div class='input-group input-group-select select-terminal'>";
                    templates += "<select name='terminal_simcard' id='terminal_simcard' class='ajax-search-terminal selectpicker jqTransformSelectWrapperSimcard' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php echo _l('dropdown_non_selected_tex'); ?>' >";
                    if(request_terminal_id!=''){
                        templates += "<option value='"+request_terminal_id+"' selected>"+request_terminal_serial+"</option>";
                    }templates += "<option value=''></option>";
                    templates += "</select>";
                    templates += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
                    templates += "<a href='#' ><i class='fa fa-plus'></i></a>";
                    templates += "</div>";
                    templates += "</div>";
                    $(".terminales_simcard").html(templates);

                    init_ajax_search_terminal('terminal', '#terminal_simcard.ajax-search-terminal', {
                        tickets_contacts: true,
                        contact_userid: id
                    });

                    $(".jqTransformSelectWrapperSimcard").on("click",
                        '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])',
                        function(event) {
                            var t = $('#terminal_simcard').val();
                            $.post(admin_url + "tickets/ticket_change_data_terminal/", {
                                contact_id: t
                            }).done(
                                function(e) {
                                    res = JSON.parse(e);
                                    if (res.contact_data) {
                                        console.log(res.contact_data);
                                        //SerializeArrayTerminalExists(res.contact_data);
                                    }
                                
                                }
                            )
                        });
                },
            }
            terminal[operation]();    
            // init_ajax_search_terminal('terminal', '#terminal.ajax-search-terminal', {
            //             tickets_contacts: true,
            //             contact_userid: id
            // });
            console.log('request_terminal_id: '+request_terminal_id);
            console.log('antes de llamar al init ajax search');
            init_ajax_search_2(type_search,'#terminal.ajax-search-terminal', {
                        tickets_contacts: true,
                        contact_userid: id
                        //term_id: request_terminal_id
            },request_terminal_id);
            console.log('DESPUES de llamar al init ajax search');

            $(".jqTransformSelectWrapper").on("click",
                        '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])',
                        function(event) {
                            var t = $('#terminal').val();
                            $.post(admin_url + "tickets/ticket_change_data_terminal/", {
                                contact_id: t
                            }).done(
                                function(e) {
                                    res = JSON.parse(e);
                                    if (res.contact_data) {
                                        console.log(res.contact_data);
                                        //SerializeArrayTerminalExists(res.contact_data);
                                    }
                                
                                }
                            )
            });      
            
        }            
        

        

        $(".type_operation").change(function() {
            //console.log ('se llam a changeimput por el evento on change de tipo operacion.');
            chage_imputs();
        });        
        
    

        $("#rif_reason_change").on('change',function() {

            var formData = new FormData();
            var rif = $("#type_rif").val() + $("#rif_reason_change").val();
            //console.log(rif);

            if (typeof(csrfData) !== 'undefined') {
                formData.append(csrfData['token_name'], csrfData['hash']);
            }

            formData.append("rif", rif);

            $.ajax({
                method: 'post',
                data: formData,
                contentType: false,
                processData: false,
                url: admin_url + "requests/rif_exists",
                dataType: 'json'
            }).success(function(value){
                console.log(value);
                if(value){
                    $("#business_name_reason_change").val(value['company']); 
                    $("#phone_reason_change").val(value['phonenumber']); 
                }
                if(value == null){
                    alert_float('danger', 'Cliente no existe.');
                    $("#business_name_reason_change").val(''); 
                    $("#phone_reason_change").val(''); 
                } 
            });

        });

    });
    </script>
