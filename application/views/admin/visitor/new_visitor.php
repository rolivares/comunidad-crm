<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>

<div id="wrapper" >
   <div class="content">
   <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'visitor-form')) ;?>
         <div class="col-md">
            <div class="panel_s">
               <div class="panel-body">
                  <div>
                     <div class="tab-content">
                     <h4 class="customer-profile-group-heading"><?php echo _l('visitor'); ?></h4>
                        <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                    <?php echo render_select('type_rif', $type_rif,array('type_rif','type_rif'),'type_rif');?>
                                    </div>
                                    <div class="col-md-3">
                                    <?php echo render_input('rif', 'rif','','number',array('required'=>true)); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo render_input('business', 'business','','text',array('required'=>true)); ?>
                                        <input type="hidden" name="userid" id="userid" />
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <?php echo render_input('name_and_surname', 'name_and_surname','','text',array('required'=>true)); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?php echo render_input('surname', 'client_lastname','','text',array('required'=>true)); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo render_input('identification_card', 'identification_card','','number',array('min'=>1)); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo render_select('typing', $typing,array('id','typing'),'typing');?>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <?php echo render_input('correo', 'correo','','text',array('required'=>true)); ?>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo render_input('contact_number', 'contact_number','','number',array('required'=>true)); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo render_input('place', 'place','','text',array('required'=>true)); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- <?php echo render_input('check_in', 'check_in','','text',array('required'=>true)); ?> -->
                                        <?php echo render_datetime_input('check_in','check_in','',array('data-date-min-date'=>_d(date('Y-m-d')))); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo render_select('sale_agent',$staff,array('staffid',array('firstname','lastname')),'sale_agent_string');?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- <?php echo render_input('attention_time', 'attention_time','','text',array('required'=>true)); ?> -->
                                        <?php echo render_datetime_input('attention_time','attention_time','',array('data-date-min-date'=>_d(date('Y-m-d')))); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <!-- <?php echo render_input('management_time', 'management_time','','text',array('required'=>true)); ?> -->
                                        <?php echo render_datetime_input('management_time','management_time','',array('data-date-min-date'=>_d(date('Y-m-d')))); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <?php echo render_select('bank', $banks,array('id','banks'),'bank',array('required'=>true));?>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <?php echo render_select('fountain',$staff,array('staffid',array('firstname','lastname')),'fountain');?>
                                    </div> -->
                                <div class="col-md-6 leads-filter-column">
                                    <?php echo render_select('fountain', $sources,array('id','name'),'fountain');?>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo render_textarea('description', 'description',''); ?>
                                    </div>
                                    
                                </div>

                                
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button> -->
                                    <!-- <button href="#" onclick="save_visitor(); return false;" style="margin-top: 1em;" class="btn btn-info"><?php echo _l('submit'); ?></button>  -->
                                    <button type="submit" class="btn btn-info" id="submit"><?php echo _l('submit'); ?></button>
                                    <!-- <button href="#"  return false;" style="margin-top: 1em;" class="btn btn-info"><?php echo _l('close'); ?></button>  -->
                                </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    
</div>
<?php echo form_close(); ?>
<?php init_tail(); ?>
<!-- <?php $this->load->view('admin/visitor/save_visitor_js'); ?> -->
</div>
<script>
    
    appValidateForm($('#visitor-form'),{
        type_rif:'required',
        identification_card:'required',
        rif:'required',
        fountain:'required',
        sale_agent:'required',
        typing:'required',
        surname:'required',
        name_and_surname:'required',
        correo: {
         email:true,
         required:true
       },
    
    },save_visitor);

    function save_visitor() {

        var formData = new FormData();

        var name_and_surname = $("#name_and_surname").val();
        var identification_card = $("#identification_card").val();
        var admission_date = $("#admission_date").val();
        var business = $("#business").val();
        var rif = $("#type_rif").val() + $("#rif").val();
        var correo = $("#correo").val();
        var contact_number = $("#contact_number").val();
        var place = $("#place").val();
        var check_in = $("#check_in").val();
        var sale_agent = $("#sale_agent").val();
        var attention_time = $("#attention_time").val();
        var management_time = $("#management_time").val();
        var bank = $("#bank").val();
        var fountain = $("#fountain").val();
        var description = $("#description").val();
        var typing = $("#typing").val();
        var userid = $("#userid").val();
        var surname = $("#surname").val();




        //console.log(tasa);
        if (typeof(csrfData) !== 'undefined') {
            formData.append(csrfData['token_name'], csrfData['hash']);
        }

        formData.append("name_and_surname", name_and_surname);
        formData.append("identification_card", identification_card);
        formData.append("admission_date", admission_date);
        formData.append("business", business);
        formData.append("rif", rif);
        formData.append("correo", correo);
        formData.append("contact_number", contact_number);
        formData.append("place", place);
        formData.append("check_in", check_in);
        formData.append("sale_agent", sale_agent);
        formData.append("attention_time", attention_time);
        formData.append("management_time", management_time);
        formData.append("bank", bank);
        formData.append("fountain", fountain);
        formData.append("description", description);
        formData.append("typing", typing);
        formData.append("userid", userid);
        formData.append("surname", surname);
        
        //console.log(formData);
        //console.log(admin_url + 'invoices/save_tasa');

        $("#submit").prop("disabled", true); 

        $.ajax({
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            url: admin_url + 'visitor/create_new_visitor',
            dataType: 'json'
            
        }).done(function(response){
            //console.log(response);    
                if (response.status) {
                    //console.log(response.status);
                    window.location.href = admin_url + 'visitor';
                    alert_float('success', response.message);                    
                }
        }).fail(function(error){
            alert_float('danger', JSON.parse(error.responseText));
            //console.log("error");
        });

    }

    $("#rif").on('change',function() {

    
        var formData = new FormData();

        var rif = $("#type_rif").val() + $("#rif").val();
        console.log(rif);
        

        if (typeof(csrfData) !== 'undefined') {
            formData.append(csrfData['token_name'], csrfData['hash']);
        }

        formData.append("rif", rif);

        $.ajax({
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            url: admin_url + "visitor/rif_exists",
            dataType: 'json'
        }).success(function(value){
            console.log(value);
            if(value){
                $("#business").val(value['company']); 
                $("#userid").val(value['userid']); 
            }
            if(value == null){
                alert_float('warning', 'El cliente no existe');
                $("#business").val(''); 
                $("#userid").val(''); 
            } 
        });

    });

</script>