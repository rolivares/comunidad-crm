<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <div id="date-range" class="mbot15">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="report-from"
                                        class="control-label"><?php echo _l('report_sales_from_date'); ?></label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control datepicker" id="report-from"
                                            name="report-from">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar calendar-icon"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="report-to"
                                        class="control-label"><?php echo _l('report_sales_to_date'); ?></label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control datepicker" disabled="disabled"
                                            id="report-to" name="report-to">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar calendar-icon"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-3">
                                    <?php echo render_select('tickets', $ticketsid,array('ticketid','ticketid'),'tickets');?>
                                </div>
                                <div class="col-md-3">
                                    <label for="assigned" class="control-label">
                                        <?php echo _l('terminal'); ?>
                                    </label>
                                    <select name="terminals" required="true"
                                        id="terminals"
                                        class="selectpicker Customer_Name_Spanisdescription" data-width="100%"
                                        data-live-search="true"
                                        data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                        <option value="" selected>Nada seleccionado</option>
                                        <?php  foreach ($terminals as $key => $terminal): ?>
                                            <?php  foreach ($terminal as $key => $value): ?>
                                                <option value="<?= $value->id ?>"> <?= $value->serial ?>
                                                </option>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    </select>
                                </div> -->

                                <div class="clearfix"></div>
                                <hr class="hr-panel-heading" />
                            </div>
                        </div>
                        <table class="table dt-table-loading table-expenses">
                            <thead>
                                <tr>
                                    <th><?php echo _l('Tickets'); ?></th>
                                    <th><?php echo _l('Tema'); ?></th>
                                    
                                    <th><?php echo _l('Departamento'); ?></th>
                                    <th><?php echo _l('Servicio'); ?></th>
                                    <th><?php echo _l('Cliente'); ?></th>
                                    <!-- <th><?php echo _l('Cliente'); ?></th> -->
                                    <th><?php echo _l('Estado'); ?></th>
                                    <th><?php echo _l('Prioridad'); ?></th>
                                    <th><?php echo _l('Última modificación'); ?></th>
                                    <th><?php echo _l('Terminales'); ?></th> 
                                     <th><?php echo _l('Fecha'); ?></th> 
                                      
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 
                                    <td></td> 
                                    <!-- <td></td> -->

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>

<script>
$(function() {

    var report_from = $('input[name="report-from"]');
    var report_to = $('input[name="report-to"]');
    var filter_selector_helper =
        '.expenses-filter-year,.expenses-filter-month-wrapper,.expenses-filter-month,.months-divider,.years-divider';

    var Ingenious_ServerParams = {};
    $.each($('._hidden_inputs._filters input'), function() {
        Ingenious_ServerParams[$(this).attr('name')] = '[name="' + $(this).attr('name') + '"]';
    });


    Ingenious_ServerParams['report_from'] = '[name="report-from"]';
    Ingenious_ServerParams['report_to'] = '[name="report-to"]';

    initDataTable('.table-expenses', window.location.href, 'undefined', 'undefined', Ingenious_ServerParams, [8,
        'desc'
    ]);

    report_from.on('change', function() {
        var val = $(this).val();
        var report_to_val = report_to.val();
        if (val != '') {
            report_to.attr('disabled', false);
            $(filter_selector_helper).removeClass('active').addClass('hide');
            $('input[name^="year_"]').val('');
            $('input[name^="expenses_by_month_"]').val('');
        } else {
            report_to.attr('disabled', true);
        }

        if ((report_to_val != '' && val != '') || (val == '' && report_to_val == '') || (val == '' &&
                report_to_val != '')) {
            $('.table-expenses').DataTable().ajax.reload();
        }

        if (val == '' && report_to_val == '' || report_to.is(':disabled') && val == '') {
            $(filter_selector_helper).removeClass('hide');
        }
    });

    report_to.on('change', function() {
        var val = $(this).val();
        if (val != '') {
            $('.table-expenses').DataTable().ajax.reload();
        }
    });


    $('select[name="currencies"]').on('change', function() {
        $('.table-expenses').DataTable().ajax.reload();
    });
})
</script>
</body>

</html>