<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>

<div id="wrapper" >
   <div class="content">
   <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'rate-form')) ;?>
         <div class="col-md">
            <div class="panel_s">
               <div class="panel-body">
                  <div>
                     <div class="tab-content">
                     <h4 class="customer-profile-group-heading"><?php echo _l('tasa'); ?></h4>
                        <div class="col-md-12">
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <?php echo render_input('tasa', 'tasa','','number',array('required'=>true)); ?>
                                    </div>
                                </div>
                             
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button> -->
                                    <!-- <button href="#" onclick="save_tasa(); return false;" style="margin-top: 1em;" class="btn btn-info"><?php echo _l('submit'); ?></button> -->
                                    <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                                </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
<?php init_tail(); ?>
<!-- <?php $this->load->view('admin/invoices/save_tasa_js'); ?> -->
</div>
<script>
    appValidateForm($('#rate-form'),{
        tasa:'required',
    },save_tasa);
 
    function save_tasa() {

        var formData = new FormData();

        var tasa = $("#tasa").val();
        //console.log(tasa);
        if (typeof(csrfData) !== 'undefined') {
            formData.append(csrfData['token_name'], csrfData['hash']);
        }
        formData.append("tasa", tasa);

        //console.log(formData);
        //console.log(admin_url + 'invoices/save_tasa');
        $.ajax({
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            url: admin_url + 'invoices/save_tasa',
            dataType: 'json'
            
        }).done(function(response){
            //console.log(response);    
                if (response.status) {
                    //console.log(response.status);
                    window.location.href = admin_url + 'rate';
                    alert_float('success', response.message);
                    $("#tasa").val('');
                }
        }).fail(function(error){
            alert_float('danger', JSON.parse(error.responseText));
            //console.log("error");
        });

    }
</script>