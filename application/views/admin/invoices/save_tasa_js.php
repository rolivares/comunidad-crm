<script>

function save_tasa() {

        var formData = new FormData();

        var tasa = $("#tasa").val();
        //console.log(tasa);
        if (typeof(csrfData) !== 'undefined') {
            formData.append(csrfData['token_name'], csrfData['hash']);
        }
        formData.append("tasa", tasa);

        //console.log(formData);
        //console.log(admin_url + 'invoices/save_tasa');
        $.ajax({
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            url: admin_url + 'invoices/save_tasa',
            dataType: 'json'
            
        }).done(function(response){
            //console.log(response);    
                if (response.status) {
                    //console.log(response.status);
                    alert_float('success', response.message);
                    $("#tasa").val('');
                }
        }).fail(function(error){
            alert_float('danger', JSON.parse(error.responseText));
            //console.log("error");
        });
   
}

</script>