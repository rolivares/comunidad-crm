<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Simcard_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    public function get_operator($id)
    {  
        $this->db->select('id, operadora');
        $this->db->where('id', $id);
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'simcard')->result_array(); //get_copiled_select();   
    }

    public function get_terminal_by_simcard($id_simcard)
    {
    $res = $this->db->select('id','simcard')
        ->from(db_prefix() . 'terminals')
        ->where(db_prefix() . 'terminals.simcard', $id_simcard)
        ->get()->row();
        //->get_compiled_select();
         //var_dump($res); die; 

        return $res;
    }

    //funcion para cambiar  las operadora de la simvard asociada a una terminal
    public function change_terminal_operator($id_terminal, $operator)
    {
        $this->db->where('id', $id_terminal);
        $this->db->update(db_prefix() . 'terminals', [
            'operadoraid' => $operator
        ]);
        //var_dump($this->db->affected_rows()); die;         
        $res  = false;
        if ($this->db->affected_rows() > 0) {
            $res  = true;
        }
        return  $res;
    }

    public function get_avalables_simcards($id = '')
    {
                  
        $sql="SELECT " .db_prefix() ."simcard.id, " .db_prefix() ."simcard.serialsimcard, " .db_prefix() ."simcard.operadora, " .db_prefix() ."simcard.status, CONCAT(" .db_prefix() ."simcard.serialsimcard, ' - '," .db_prefix() ."operadora.operadora) as serialyoperadora ";
        $sql.=" FROM ".db_prefix() ."simcard  LEFT JOIN " .db_prefix() ."operadora ON " .db_prefix() ."operadora.operadoraid = " .db_prefix() ."simcard.operadora";
        $sql.=" where  " .db_prefix() ."simcard.id not in (select simcard from " .db_prefix() ."terminals where simcard is not null )";
         
        if (is_numeric($id)) {
            $sql.=" OR  ".db_prefix() ."simcard.id = '".$id."'";                
        }
        
        //var_dump($sql); die;
        $query = $this->db->query($sql);
        //var_dump($query->result_array()); die;

        return $query->result_array(); 
    }

    public function validate_simcard_available($id_sim = '', $id_terminal = '')
    { // se valida que la                   
        $sql="select simcard from " .db_prefix() ."terminals where simcard is not null  and  ".db_prefix() ."terminals.simcard = ".$id_sim;
        
        if (is_numeric($id_terminal)) {
            $sql.=" and ".db_prefix() ."terminals.id != ".$id_terminal;              
        }
        $query = $this->db->query($sql);
        //var_dump($query->row()); die;
        return $query->row(); 

        // $this->db->order_by('id', 'asc');
        // return $this->db->get(db_prefix() . 'simcard')->result_array();
    }

    public function get_sim_operator($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'simcard')->row();
        }

        $this->db->where('mostrar_en_simcard', 1);
        $this->db->order_by('operadoraid', 'asc');

        return $this->db->get(db_prefix() . 'operadora')->result_array();
    }

    public function search_simcard($q, $limit = 0, $where = ''){
        $result = [
            'result'         => [],
            'type'           => 'simcard',
            'search_heading' => _l('customer_contacts'),
        ];

        $have_assigned_customers        = have_assigned_customers();
        $have_permission_customers_view = has_permission('customers', '', 'view');

        if ($have_assigned_customers || $have_permission_customers_view) {
            // Contacts
            $this->db->select(implode(',', prefixed_table_fields_array(db_prefix() . 'simcard')).', '. db_prefix() . 'operadora.operadora as nombreopradora');

            $this->db->from(db_prefix() . 'simcard');
            $this->db->join(db_prefix() . 'operadora', '' . db_prefix() . 'operadora.operadoraid = ' . db_prefix() . 'simcard.operadora', 'left');


            $this->db->where('(serialsimcard LIKE "%' . $this->db->escape_like_str($q) . '%" ESCAPE \'!\')');

            if ($where != '') {
                $this->db->where($where);
            }

            if ($limit != 0) {
                $this->db->limit($limit);
            }

            $this->db->order_by('id', 'ASC');
            //var_dump($this->db->get_compiled_select());die;
            $result['result'] = $this->db->get()->result_array();
             //echo "<pre>";
             //var_dump($result['result']); die; 
            
        }
        return $result;
    }

    public function search_availables_simcards(){
       

            $this->db->select(implode(',', prefixed_table_fields_array(db_prefix() . 'simcard')).', '. db_prefix() . 'operadora.operadora as nombreopradora');

            $this->db->from(db_prefix() . 'simcard');
            $this->db->join(db_prefix() . 'operadora', '' . db_prefix() . 'operadora.operadoraid = ' . db_prefix() . 'simcard.operadora', 'left');


            $this->db->where('simcard.id NOT IN (SELECT simcard FROM '.db_prefix().'terminals where simcard is not null )');


            $this->db->order_by('id', 'ASC');
            //var_dump($this->db->get_compiled_select());die;
            $result = $this->db->get()->result_array();
             //echo "<pre>";
             //var_dump($result['result']); die; 
            
        return $result;
    }


    
   
}
