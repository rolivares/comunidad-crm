<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Terminal_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        // $this->db->insert(db_prefix() . 'visitor', $data);
        // $insert_id = $this->db->insert_id();
        // if ($insert_id) {
        //     return true;
        // }
        // return false;
        //var_dump($data); die;
        $datecreated = date('Y-m-d');
        $this->db->insert(db_prefix() . 'visitor', [
            'name_and_surname'       =>  $data['name_and_surname'],
            'identification_card'       =>  $data['identification_card'],
            'admission_date'  =>  $data['admission_date'],
            'business'  =>  $data['business'],
            'rif'  =>  $data['rif'],
            'correo'  =>  $data['correo'],
            'contact_number'  =>  $data['contact_number'],
            'place'  =>  $data['place'],
            'check_in'  =>  $data['check_in'],
            'sale_agent'  =>  $data['sale_agent'],
            'attention_time'  =>  $data['attention_time'],
            'management_time'  =>  $data['management_time'],
            'bank'  =>  $data['bank'],
            'fountain'  =>  $data['fountain'],
            'description'  =>  $data['description'],
            'reason_fo_visit'  =>  $data['typing'],
            'admission_date'  =>  $datecreated,
            'surname'  =>  $data['surname'],            

        ]);
        return true;
    }

    public function add_contacts($data)
    {
        
        //var_dump($data); die;
        $datecreated = date('Y-m-d');
        $this->db->insert(db_prefix() . 'contacts', [
            'userid'       =>  $data['userid'],
            'is_primary'       =>  '1',
            'firstname'  =>  $data['name_and_surname'],
            'lastname' => $data['surname'],
            'email'  =>  $data['correo'],
            'phonenumber'  =>  $data['contact_number'],
            'password' => app_hash_password('123456'),
            'active' => '1',
            'invoice_emails' => '1',
            'estimate_emails' => '1',
            'credit_note_emails' => '1',
            'contract_emails' => '1',
            'task_emails' => '1',
            'project_emails' => '0',
            'ticket_emails' => '1',
            'datecreated'  =>  $datecreated,
        ]);
        return true;
    }

    public function get_contacts($data)
    {

        $query = $this->db->select('id')
                          ->from('contacts')
                          ->where(db_prefix(). 'contacts.userid', $data)
                          //->get_compiled_select();
                          ->get()->row();

        return $query;
    }


    public function get_typing()
    {
        $this->db->select('typing,id');
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'typing_visitor')->result_array();  
    }
    
    public function get_type_rif()
    {
        $this->db->select('type_rif,id');
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'type_rif')->result_array();  
    }

    public function get_rif_clients($rif)
    {
        $this->db->select('company, userid');
        $this->db->where('rif_number', $rif);
        $data = $this->db->get(db_prefix() . 'clients')->row(); 
        return $data ;

    }
    public function get_availables_terminals()
    {  //$this->db->where('userid', $userid);
        //$this->db->where('status', '1');
        $this->db->select('id, serial');
        $this->db->where('userid = 0 and status = 3');
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'terminals')->result_array(); //get_copiled_select();   
    }

    public function search_current_and_availables_terminals($id='', $q='')
    {          
        $result = [
            'result'         => [],
            'type'           => 'terminal',
            'search_heading' => _l('customer_contacts'),
        ]; 

         $where="(userid = 0 and status = 3)";
         $w2=' serial LIKE "%' . $this->db->escape_like_str($q) . '%"';

         //se limita la busqueda segun los parametros recibidos
         if($id!=''){
           $where="(userid = 0 and status = 3) or(id='".$id."')";
           if($q!=''){
                $where="((userid = 0 and status = 3) AND".$w2." ) or(id='".$id."' AND ".$w2.")";
            }
         }elseif($q!=''){
            $where.= ' AND '.$w2;
         }     
 
        $query = $this->db->select('id, serial')
        ->from(db_prefix() . 'terminals')
        ->where($where)  
        ->order_by('id', 'asc')
        //->get_compiled_select();
        ->get()->result_array();
        //return $query;                 
        //var_dump( $query); die;
        $result['result'] = $query;

        return $result;
    }   

   
    public function  search_current_and_uninstall_terminals($id='',$userid='',$q=''){
        $result = [
            'result'         => [],
            'type'           => 'terminal',
            'search_heading' => _l('customer_contacts'),
        ]; 

        if ($userid != ''){ 
            $w1="(userid = ".$userid." and status = 2)";
        }else{ $w1="(status = 2)";
        }
         $w2=' serial LIKE "%' . $this->db->escape_like_str($q) . '%"';

         //se limita la busqueda segun los parametros recibidos
         if($id!=''){
           $where= $w1 ." OR (id='".$id."')";
           if($q!=''){
                $where="((" .$w1 ." AND ".$w2." ) or (id='".$id."' AND ".$w2."))";
            }
         }elseif($q!=''){
            $where = $w1 ." AND ".$w2;
         }     
 
        $query = $this->db->select('id, serial')
        ->from(db_prefix() . 'terminals')
        ->where($where)  
        ->order_by('id', 'asc')
        //->get_compiled_select();
        ->get()->result_array();
        //return $query;                 
        //var_dump( $query); die;
        $result['result'] = $query;

        return $result;
    }  

     public function search_current_and_own_terminals($id='',$userid='', $q='')
    {          
        $result = [
            'result'         => [],
            'type'           => 'terminal',
            'search_heading' => _l('customer_contacts'),
        ]; 

        $where="(userid = ".$userid." and status = 1)";
         $w2=' serial LIKE "%' . $this->db->escape_like_str($q) . '%"';

         //se limita la busqueda segun los parametros recibidos
         if($id!=''){
            $where="(userid = ".$userid." and status = 1) or(id='".$id."')";
           if($q!=''){
                $where="((userid = ".$userid." and status = 1) AND".$w2." ) OR (id='".$id."' AND ".$w2.")";
            }
         }elseif($q!=''){
            $where.= ' AND '.$w2;
         }        
        

        $query = $this->db->select('id, serial')
        ->from(db_prefix() . 'terminals')
        ->where($where)  
        ->order_by('id', 'asc')
        //->get_compiled_select();
        ->get()->result_array();
        //return $query;                 
        //var_dump( $query); die;
        $result['result'] = $query;

        return $result;

    }

    public function get_terminal_model($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('modelid', $id);

            return $this->db->get(db_prefix() . 'terminal_models')->row();
        }

        $this->db->order_by('modelid', 'asc');

        $this->db->where('status', 1);
        $this->db->where('mostrar_en_terminals', 1);
        
        return $this->db->get(db_prefix() . 'terminal_models')->result_array();
    }
    public function get_terminal($id = '')
    {
        
        //var_dump($id); die;
        if (is_numeric($id)) {
            $query = $this->db->select(implode(',', prefixed_table_fields_array(db_prefix() . 'terminals')) . ',tblsimcard.serialsimcard, tblbanks.banks as bank, tblterminal_models.model, tbloperadora.operadora, CONCAT(tblsimcard.serialsimcard," - ",tbloperadora.operadora) as serialyOperadora')
                ->from(db_prefix() .'terminals')
                ->join(db_prefix() . 'simcard', '' . db_prefix() . 'simcard.id=' . db_prefix() . 'terminals.simcard', 'left')
                ->join(db_prefix() . 'banks', '' . db_prefix() . 'banks.id=' . db_prefix() . 'terminals.banks', 'left')
                ->join(db_prefix() . 'terminal_models', '' . db_prefix() . 'terminal_models.modelid=' . db_prefix() . 'terminals.modelid', 'left')
                ->join(db_prefix() . 'operadora', '' . db_prefix() . 'operadora.operadoraid=' . db_prefix() . 'terminals.operadoraid', 'left')
                ->where(db_prefix().'terminals.id', $id)
                ->order_by('id', 'asc')
                ->get()->result_array();
                //->get_compiled_select();
                
            return current($query);
        }
    }

   
}
