<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Visitor_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        // $this->db->insert(db_prefix() . 'visitor', $data);
        // $insert_id = $this->db->insert_id();
        // if ($insert_id) {
        //     return true;
        // }
        // return false;
        //var_dump($data); die;
        $datecreated = date('Y-m-d');
        $this->db->insert(db_prefix() . 'visitor', [
            'name_and_surname'       =>  $data['name_and_surname'],
            'identification_card'       =>  $data['identification_card'],
            'admission_date'  =>  $data['admission_date'],
            'business'  =>  $data['business'],
            'rif'  =>  $data['rif'],
            'correo'  =>  $data['correo'],
            'contact_number'  =>  $data['contact_number'],
            'place'  =>  $data['place'],
            'check_in'  =>  $data['check_in'],
            'sale_agent'  =>  $data['sale_agent'],
            'attention_time'  =>  $data['attention_time'],
            'management_time'  =>  $data['management_time'],
            'bank'  =>  $data['bank'],
            'fountain'  =>  $data['fountain'],
            'description'  =>  $data['description'],
            'reason_fo_visit'  =>  $data['typing'],
            'admission_date'  =>  $datecreated,
            'surname'  =>  $data['surname'],
            

        ]);
        return true;
    }

    public function add_contacts($data)
    {
        
        //var_dump($data); die;
        $datecreated = date('Y-m-d');
        $this->db->insert(db_prefix() . 'contacts', [
            'userid'       =>  $data['userid'],
            'is_primary'       =>  '1',
            'firstname'  =>  $data['name_and_surname'],
            'lastname' => $data['surname'],
            'email'  =>  $data['correo'],
            'phonenumber'  =>  $data['contact_number'],
            'password' => app_hash_password('123456'),
            'active' => '1',
            'invoice_emails' => '1',
            'estimate_emails' => '1',
            'credit_note_emails' => '1',
            'contract_emails' => '1',
            'task_emails' => '1',
            'project_emails' => '0',
            'ticket_emails' => '1',
            'datecreated'  =>  $datecreated,
        ]);
        return true;
    }

    public function get_contacts($data)
    {

        $query = $this->db->select('id')
                          ->from('contacts')
                          ->where(db_prefix(). 'contacts.userid', $data)
                          //->get_compiled_select();
                          ->get()->row();

        return $query;
    }


    public function get_typing()
    {
        $this->db->select('typing,id');
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'typing_visitor')->result_array();  
    }
    
    public function get_type_rif()
    {
        $this->db->select('type_rif,id');
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'type_rif')->result_array();  
    }

    public function get_rif_clients($rif)
    {
        $this->db->select('company, userid');
        $this->db->where('rif_number', $rif);
        $data = $this->db->get(db_prefix() . 'clients')->row(); 
        return $data ;

    }

   
}
