

ALTER TABLE `tblclients` CHANGE `address` `address` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `tblclients` ADD `dateReplica` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `addedfrom`;

ALTER TABLE `tblclients` ADD `rif_number` VARCHAR(20) NOT NULL AFTER `userid`;

ALTER TABLE `tblitems` CHANGE `long_description` `long_description` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `tblitems` ADD `dateReplica` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `color`;

ALTER TABLE `tblitems` CHANGE `description` `description` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `tblitems` ADD `status_profit` INT(2) NOT NULL AFTER `dateReplica`;

ALTER TABLE `tblclients` CHANGE `phonenumber` `phonenumber` VARCHAR(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;