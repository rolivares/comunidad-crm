--
-- Estructura de tabla para la tabla `tblservices`
--
DROP TABLE `tblservices`;
CREATE TABLE `tblservices` (
  `serviceid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `coste` float NOT NULL,
  `services_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Volcado de datos para la tabla `tblservices`
--

INSERT INTO `tblservices` (`serviceid`, `name`, `coste`, `services_status`) VALUES
(1, 'Diagnóstico', 15, 1),
(2, 'S.R. Nivel I', 70, 1),
(3, 'S.R. Nivel II', 90, 1),
(4, 'S.R. Nivel III', 120, 1),
(5, 'S.R. Nivel IV', 160, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblservices`
--
ALTER TABLE `tblservices` ADD PRIMARY KEY (`serviceid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblservices`
--
ALTER TABLE `tblservices` MODIFY `serviceid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

