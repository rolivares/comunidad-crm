CREATE TABLE `tblsimcard` (
  `id` int(11) NOT NULL,
  `serial` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `operadora` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

ALTER TABLE `tblsimcard`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tblsimcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;
