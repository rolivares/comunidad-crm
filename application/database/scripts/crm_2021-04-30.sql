--
-- Estructura de tabla para la tabla `tblrate`
--

CREATE TABLE `tblrate` (
  `id` int(5) NOT NULL,
  `tasa_c` decimal(14,2) NOT NULL,
  `tasa_v` decimal(14,2) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblrate`
--

INSERT INTO `tblrate` (`id`, `tasa_c`, `tasa_v`, `date`) VALUES
(1, '2647851.66', '2647851.66', '2021-04-27');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblrate`
--
ALTER TABLE `tblrate`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblrate`
--
ALTER TABLE `tblrate`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

--
-- ALTER TABLE tblinvoices
--

ALTER TABLE `tblinvoices` ADD `totalbsf` DECIMAL(14,2) NOT NULL AFTER `total`;

ALTER TABLE `tblinvoices` ADD `tasadate` DECIMAL(14,2) NOT NULL AFTER `totalbsf`;

--
--TRUNCATE TABLE tblitems 
--


TRUNCATE TABLE `tblitems`

INSERT INTO `tblitems` (`id`, `description`, `long_description`, `rate`, `tax`, `tax2`, `unit`, `group_id`, `commodity_code`, `commodity_barcode`, `commodity_type`, `warehouse_id`, `origin`, `color_id`, `style_id`, `model_id`, `size_id`, `unit_id`, `sku_code`, `sku_name`, `purchase_price`, `sub_group`, `commodity_name`, `color`, `dateReplica`, `status_profit`) VALUES
(1, 'POS IWL220 INGENICO', 'POS IWL220 INGENICO', '720.00', NULL, NULL, NULL, 1, '00019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:11', 0),
(2, 'TARJETA ANTENA GPRS Y07 V2', 'TARJETA ANTENA GPRS Y07 V2', '12.00', NULL, NULL, NULL, 1, '0022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(3, 'TARJETA ANTENA GPRS V2 IW', 'TARJETA ANTENA GPRS V2 IW', '34.00', NULL, NULL, NULL, 1, '00220-SERV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(4, 'TECLADO ELASTOMETRICO IWL220', 'TECLADO ELASTOMETRICO IWL220', '8.00', NULL, NULL, NULL, 1, '0296129177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(5, 'DRIVER IMPRESORA 4752', 'DRIVER IMPRESORA 4752', '10.00', NULL, NULL, NULL, 1, '188954752', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(6, 'DRIVER IMPRESORA 4543', 'DRIVER IMPRESORA 4543', '9.00', NULL, NULL, NULL, 1, '189054543', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(7, 'CONECTOR SMART CARD DETECT 8PIN (LECTOR DE CHIP)', 'CONECTOR SMART CARD DETECT 8PIN (LECTOR DE CHIP)', '17.00', NULL, NULL, NULL, 1, '189418424', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(8, 'RODILLO IMPRESORA ICT', 'RODILLO IMPRESORA ICT', '22.00', NULL, NULL, NULL, 1, '192000053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(9, 'BATERIA DE LITHIUM 3V 225MAH LF', 'BATERIA DE LITHIUM 3V 225MAH LF', '50.00', NULL, NULL, NULL, 1, '192001492', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 1),
(10, 'BATERIA  LITHIUM 3V IWL 225MAH LF V', 'BATERIA  LITHIUM 3V IWL 225MAH LF V', '50.00', NULL, NULL, NULL, 1, '1920014920', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(11, 'CONECTOR USB', 'CONECTOR USB', '8.00', NULL, NULL, NULL, 1, '192022318', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 1),
(12, 'RODILLO DE IMPRESORA IWL', 'RODILLO DE IMPRESORA IWL', '22.00', NULL, NULL, NULL, 1, '1920533630', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(13, 'CIRCUITO INTEGRADO 58 II BGA217', 'CIRCUITO INTEGRADO 58 II BGA217', '13.00', NULL, NULL, NULL, 1, '2935116691AB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:12', 0),
(14, 'TAPAS COMPLEMENTO', 'TAPAS COMPLEMENTO', '8.00', NULL, NULL, NULL, 1, '295003069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:13', 0),
(15, 'MICA CUBRE DISPLAY ICT220 X07', 'MICA CUBRE DISPLAY ICT220 X07', '14.00', NULL, NULL, NULL, 1, '295004748AB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:13', 0),
(16, 'IMPRESORA TERMICA IWL', 'IMPRESORA TERMICA IWL', '48.00', NULL, NULL, NULL, 1, '295010709AB0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:13', 0),
(17, 'CABEZAL BANDA MAGNETICA 1.2.3 IWL', 'CABEZAL BANDA MAGNETICA 1.2.3 IWL', '14.00', NULL, NULL, NULL, 1, '296100107AB0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:13', 1),
(18, 'PELICULA DE TECLADO IWL 2XX', 'PELICULA DE TECLADO IWL 2XX', '18.00', NULL, NULL, NULL, 1, '296101299AG0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:13', 0),
(19, 'BATERIA DE LITTIUM 3V IWL', 'BATERIA DE LITTIUM 3V IWL', '50.00', NULL, NULL, NULL, 1, '296110884', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:13', 0),
(20, 'CONJUNTO TAPA IMPRESORA IWL2XX', 'CONJUNTO TAPA IMPRESORA IWL2XX', '7.00', NULL, NULL, NULL, 1, '296117247AB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(21, 'MICA CUBRE DISPLAY IWL220', 'MICA CUBRE DISPLAY IWL220', '5.00', NULL, NULL, NULL, 1, '296122095AC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(22, 'DISPLAY IWL 250', 'DISPLAY IWL 250', '38.00', NULL, NULL, NULL, 1, '296126494AB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(23, 'TECLADO ELASTOMERICO IWL220 V2 (MARKED-MEMBRANA)', 'TECLADO ELASTOMERICO IWL220 V2 (MARKED-MEMBRANA)', '7.98', NULL, NULL, NULL, 1, '296129177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 1),
(24, 'TECLADO ELASTOMERICO IWL220 V2 (MARKED-MEMBRANA)', 'TECLADO ELASTOMERICO IWL220 V2 (MARKED-MEMBRANA)', '8.00', NULL, NULL, NULL, 1, '2961291770', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(25, 'PELICULA DEL TECLADO ICT DIMPLE PCIV2', 'PELICULA DEL TECLADO ICT DIMPLE PCIV2', '4.73', NULL, NULL, NULL, 1, '296155669', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(26, 'CABEZAL BANDA MAG ICT 220', 'CABEZAL BANDA MAG ICT 220', '20.00', NULL, NULL, NULL, 1, '296173901', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(27, 'BATERIA DE LITHIUM 3V EXTERNA IWL 220', 'BATERIA DE LITHIUM 3V EXTERNA IWL 220', '50.00', NULL, NULL, NULL, 1, 'BATEXTIWL220', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(28, 'BATERIA DE LITHIUM 3V INTERNA', 'BATERIA DE LITHIUM 3V INTERNA', '50.00', NULL, NULL, NULL, 1, 'BATIWL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 1),
(29, 'BATERIA DE LITHIUM 3V 550MAH LF 930X ICT', 'BATERIA DE LITHIUM 3V 550MAH LF 930X ICT', '10.00', NULL, NULL, NULL, 1, 'BTLI3V0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(30, 'CABEZAL BANDA MAGNETICA 1.2.3 ICT2XX', 'CABEZAL BANDA MAGNETICA 1.2.3 ICT2XX', '20.00', NULL, NULL, NULL, 1, 'CABM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 1),
(31, 'CARGADOR P/POS ICT 220', 'CARGADOR P/POS ICT 220', '45.00', NULL, NULL, NULL, 1, 'CARG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:14', 0),
(32, 'CARGA DE LLAVE', 'CARGA DE LLAVE', '25.00', NULL, NULL, NULL, 1, 'CLLAVE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:15', 0),
(33, 'CONECTOR DE ALIMENTACION I5100', 'CONECTOR DE ALIMENTACION I5100', '50.00', NULL, NULL, NULL, 1, 'CONECI5100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:15', 0),
(34, 'SIM CARD DIGITEL', 'SIM CARD DIGITEL', '25.00', NULL, NULL, NULL, 1, 'DIGI001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:15', 0),
(35, 'GUIA METALICA DE LECTOR DE BANDA ISO X930', 'GUIA METALICA DE LECTOR DE BANDA ISO X930', '23.00', NULL, NULL, NULL, 1, 'GMLBICT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:15', 1),
(36, 'POS ICT220 INGENICO', 'POS ICT220 INGENICO', '350.00', NULL, NULL, NULL, 1, 'ICT220-11P2316A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:15', 0),
(37, 'POS ICT220 INGENICO REMANOFACTURADO', 'POS ICT220 INGENICO REMANOFACTURADO', '150.00', NULL, NULL, NULL, 1, 'ICT220EM-RF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:15', 0),
(38, 'POS ICT250 INGENICO', 'POS ICT250 INGENICO', '350.00', NULL, NULL, NULL, 1, 'ICT250-11P3700B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:15', 0),
(39, 'CARGADORES IWL 220', 'CARGADORES IWL 220', '30.00', NULL, NULL, NULL, 1, 'IWL212-01B3017A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(40, 'POS IWL220 INGENICO REMANOFACTURADO', 'POS IWL220 INGENICO REMANOFACTURADO', '150.00', NULL, NULL, NULL, 1, 'IWL220EM-RF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(41, 'POS IWL250 INGENICO', 'POS IWL250 INGENICO', '720.00', NULL, NULL, NULL, 1, 'IWL250', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(42, 'POS IWL 280 INGENICO', 'POS IWL 280 INGENICO', '280.00', NULL, NULL, NULL, 1, 'IWL28C-11P3718A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(43, 'ROUTER DE CONECTIVIDAD MULTIPLE', 'ROUTER DE CONECTIVIDAD MULTIPLE', '330.24', NULL, NULL, NULL, 1, 'MIKROTIK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(44, 'SIM CARD MOVISTAR', 'SIM CARD MOVISTAR', '25.00', NULL, NULL, NULL, 1, 'MOVIS001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(45, 'PLACA DE CIRCUITO IMPRESO IWL WIREMESH', 'PLACA DE CIRCUITO IMPRESO IWL WIREMESH', '3.00', NULL, NULL, NULL, 1, 'PLACIRC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(46, 'TAPA CUBRE PAPEL IWL220', 'TAPA CUBRE PAPEL IWL220', '8.00', NULL, NULL, NULL, 1, 'RN0009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(47, 'TAPA SUPERIOR IMPRESORA IWL220 (COMPLEMENTO)', 'TAPA SUPERIOR IMPRESORA IWL220 (COMPLEMENTO)', '8.00', NULL, NULL, NULL, 1, 'RN0011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(48, 'LECTOR DE BANDA IWL250/220', 'LECTOR DE BANDA IWL250/220', '23.00', NULL, NULL, NULL, 1, 'RN0015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(49, 'TAPAS COMPLETAS IMPRESORA IWL220-296117247AB', 'TAPAS COMPLETAS IMPRESORA IWL220-296117247AB', '8.00', NULL, NULL, NULL, 1, 'RN0017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:16', 0),
(50, 'DISPLAY ICT Y07 V2 (PANTALLA)', 'DISPLAY ICT Y07 V2 (PANTALLA)', '38.00', NULL, NULL, NULL, 1, 'RN0019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(51, 'CARCAZA INFERIOR - TRACERA IWL220/IWL250', 'CARCAZA INFERIOR - TRACERA IWL220/IWL250', '8.00', NULL, NULL, NULL, 1, 'RN0020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(52, 'CARCAZA SUPERIOR DELANTERA  Y07 (P446C) IWL220/250', 'CARCAZA SUPERIOR DELANTERA  Y07 (P446C) IWL220/250', '8.00', NULL, NULL, NULL, 1, 'RN0021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(53, 'DIODO ICT220-189006133', 'DIODO ICT220-189006133', '9.00', NULL, NULL, NULL, 1, 'RN0031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(54, 'INTEGRADO IEC-189912290', 'INTEGRADO IEC-189912290', '13.00', NULL, NULL, NULL, 1, 'RN0033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(55, 'INTEGRADO ICT220-192000008', 'INTEGRADO ICT220-192000008', '13.00', NULL, NULL, NULL, 1, 'RN0035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(56, 'TRANSISTOR ICT220-188722100', 'TRANSISTOR ICT220-188722100', '6.00', NULL, NULL, NULL, 1, 'RN0036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(57, 'PUERTO MICRO USB', 'PUERTO MICRO USB', '8.00', NULL, NULL, NULL, 1, 'RN0038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(58, 'ADAPTADORES IWL', 'ADAPTADORES IWL', '10.00', NULL, NULL, NULL, 1, 'RN0039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:17', 0),
(59, 'ADAPTADORES ICT', 'ADAPTADORES ICT', '10.00', NULL, NULL, NULL, 1, 'RN0040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:18', 0),
(60, 'TARJETA SECUNDARIA IWL2X0+ V3 3G CL 2SIM 1SAM SD', 'TARJETA SECUNDARIA IWL2X0+ V3 3G CL 2SIM 1SAM SD', '34.00', NULL, NULL, NULL, 1, 'SECUNDIWL2X0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:18', 0),
(61, 'TARJETA DE CIRCUITO INTERNO', 'TARJETA DE CIRCUITO INTERNO', '17.00', NULL, NULL, NULL, 1, 'TCIRC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:18', 0),
(62, 'TECLADO SIN EL FPC ICT2XX STANDARD PCIV3', 'TECLADO SIN EL FPC ICT2XX STANDARD PCIV3', '16.00', NULL, NULL, NULL, 1, 'TESFPC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:18', 1),
(63, 'TECLADO SIN EL FPC ICT2XX STANDARD PCIV3', 'TECLADO SIN EL FPC ICT2XX STANDARD PCIV3', '16.00', NULL, NULL, NULL, 1, 'TESFPC0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:18', 0),
(64, 'CASING SHIELDING THUNDER X930', 'CASING SHIELDING THUNDER X930', '2.00', NULL, NULL, NULL, 1, 'THUNDER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:18', 0),
(65, 'MICA TAPA IMPRESORA I5100', 'MICA TAPA IMPRESORA I5100', '10.50', NULL, NULL, NULL, 1, 'TPIMPREI5100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2021-04-26 15:01:18', 1);



--
-- Alter table tblproposals
--

ALTER TABLE `tblproposals` ADD `totalbsf` DECIMAL(14,2) NOT NULL AFTER `total_tax`;

ALTER TABLE `tblproposals` ADD `tasadate` DECIMAL(14,2) NOT NULL AFTER `totalbsf`;


--
-- Alter table tblcreditnotes
--

ALTER TABLE `tblcreditnotes` ADD `totalbsf` DECIMAL(14,2) NOT NULL AFTER `total`;

ALTER TABLE `tblcreditnotes` ADD `tasadate` DECIMAL(14,2) NOT NULL AFTER `totalbsf`;

ALTER TABLE `tblinvoicepaymentrecords` ADD `record_payment_amount_received_bsf` DECIMAL(14,2) NOT NULL AFTER `amount`;


--
-- alter table tblestimates
--
ALTER TABLE `tblestimates` ADD `totalbsf` DECIMAL(14,2) NOT NULL AFTER `total`;
ALTER TABLE `tblestimates` ADD `tasadate` DECIMAL(14,2) NOT NULL AFTER `totalbsf`;