CREATE TABLE `tblstatus_accounts` (
  `id` int(10) NOT NULL,
  `name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;



INSERT INTO `tblstatus_accounts` (`id`, `name`) VALUES
(1, 'Activo'),
(2, 'Inactivo');


ALTER TABLE `tblstatus_accounts`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tblstatus_accounts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;