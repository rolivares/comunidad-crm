ALTER TABLE `tblingenico` 
CHANGE `1_part_status` `1_part_status` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI'
, CHANGE `1_part_number` `1_part_number` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `1_part_description` `1_part_description` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `1_part_cost` `1_part_cost` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `2_part_status` `2_part_status` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `2_part_number` `2_part_number` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `2_part_description` `2_part_description` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `2_part_cost` `2_part_cost` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `3_part_status` `3_part_status` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `3_part_number` `3_part_number` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `3_part_description` `3_part_description` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `3_part_cost` `3_part_cost` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `4_part_status` `4_part_status` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `4_part_number` `4_part_number` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `4_part_description` `4_part_description` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `4_part_cost` `4_part_cost` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `5_part_status` `5_part_status` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `5_part_number` `5_part_number` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `5_part_description` `5_part_description` VARCHAR(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI', 
CHANGE `5_part_cost` `5_part_cost` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT 'NI';


CREATE TABLE `tblrepair_level` (
  `id` int(10) NOT NULL,
  `repair` varchar(90) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


INSERT INTO `tblrepair_level` (`id`, `repair`) VALUES
(0, 'A'),
(1, 'B'),
(2, 'C'),
(3, 'D'),
(4, 'E'),
(5, 'I'),
(6, 'B.E.R'),
(7, 'SWAP'),
(8, 'PT');

ALTER TABLE `tblrepair_level`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tblrepair_level`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;