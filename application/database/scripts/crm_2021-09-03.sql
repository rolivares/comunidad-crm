--
-- Estructura de tabla para la tabla `tblstatus_accounts`
--

CREATE TABLE `` (
  `id` int(10) NOT Ntblstatus_accountsULL,
  `name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tblstatus_accounts`
--

INSERT INTO `tblstatus_accounts` (`id`, `name`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblstatus_accounts`
--
ALTER TABLE `tblstatus_accounts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblstatus_accounts`
--
ALTER TABLE `tblstatus_accounts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;