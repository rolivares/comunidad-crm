

ALTER TABLE `tblingenico` ADD `5_part_cost` VARCHAR(255) NOT NULL AFTER `5_part_description`;
ALTER TABLE `tblingenico` ADD `in_number` VARCHAR(255) NOT NULL AFTER `5_part_cost`, ADD `out_number` VARCHAR(255) NOT NULL AFTER `in_number`, ADD `lote_placa_box_out_number` VARCHAR(255) NOT NULL AFTER `out_number`;
ALTER TABLE `tblingenico` ADD `terminalid` INT(255) NOT NULL AFTER `tickecid`;


CREATE TABLE tblrepair_status(
	id int(11) AUTO_INCREMENT,
    repair varchar(255),
    PRIMARY key (id)
)

INSERT INTO `tblrepair_status` (id, repair) VALUES (null, 'REPAIRED'), (null, 'UNREPAIRED'), (null, 'OPEN')