
DROP TABLE IF EXISTS `tblrequests_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblrequests_sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `requests_source` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblrequests_sources`
--

LOCK TABLES `tblrequests_sources` WRITE;
/*!40000 ALTER TABLE `tblrequests_sources` DISABLE KEYS */;
INSERT INTO `tblrequests_sources` VALUES (1,'CALLCENTER',1),(2,'OFICINA',1),(3,'REDES SOCIALES',1),(4,'ALIADO PT VENEZUELA',1),(5,'ALIADO PAGO SEGURO (C. OJEDA)',1),(6,'ALIADO SERVIPOST C.',1),(7,'ALIADO INGELPAX',1),(8,'ALIADO TECNIPUNTO',1),(9,'JORNADA',1);
/*!40000 ALTER TABLE `tblrequests_sources` ENABLE KEYS */;
UNLOCK TABLES;