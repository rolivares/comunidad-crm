-- creaacion de tablas y datos iniciales para modulo de solicitudes

CREATE TABLE `tbloperations` (
  `id` int(11) NOT NULL,
  `id_operation` int(5) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `communication_required` int(11) DEFAULT NULL,
  `operator_required` int(11) DEFAULT NULL,
  `model_required` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `terminal` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `terminal_saliente` int(10) DEFAULT NULL,
  `terminal_entrante` int(10) DEFAULT NULL,
  `affiliate_code` int(11) DEFAULT NULL,
  `num_terminal` int(11) DEFAULT NULL,
  `rif_reason_change` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `business_name_reason_change` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `phone_reason_change` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `affiliate_code_reason_change` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `requests_banks_reason_change` int(5) DEFAULT NULL,
  `terminal_reason_change` int(10) DEFAULT NULL,
  `num_terminal_reason_change` int(10) DEFAULT NULL,
  `rif_bank_change` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `business_name_bank_change` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `phone_bank_change` int(15) DEFAULT NULL,
  `affiliate_code_bank_change` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `requests_banks_change` int(5) DEFAULT NULL,
  `terminal_banks_change` int(10) DEFAULT NULL,
  `num_terminal_banks_change` int(10) DEFAULT NULL,
  `sales_document` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `detail` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `observation` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `manage_code_af` int(11) DEFAULT NULL,
  `chanel` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `registration_user` int(11) DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `modified_user` int(11) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `retired_by` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `invoice_id` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


ALTER TABLE `tbloperations`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tbloperations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


--
--
--

CREATE TABLE `tbloperations_dates` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`operation_id` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
`sale` datetime DEFAULT NULL,
`payment_confirmed` datetime DEFAULT NULL,
`bank_management` datetime DEFAULT NULL,
`account_opened` datetime DEFAULT NULL,
`send_code_af` datetime DEFAULT NULL,
`bank_code_af` datetime DEFAULT NULL,
`serial_assignment` datetime DEFAULT NULL,
`send_par_loading` datetime DEFAULT NULL,
`bank_par_loading` datetime DEFAULT NULL,
`ccrd_answer` datetime DEFAULT NULL,
`parameterization` datetime DEFAULT NULL,
`cari_relocated` datetime DEFAULT NULL,
`ready` datetime DEFAULT NULL,
`retired` datetime DEFAULT NULL,
PRIMARY KEY (`id`)
);

--
--
--

CREATE TABLE `tblstatus_operations` (
  `id` int(5) NOT NULL,
  `id_status` int(5) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `color` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL DEFAULT '#28B8DA'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;


INSERT INTO `tblstatus_operations` (`id`, `id_status`, `name`, `color`) VALUES
(1, 1, 'VENTA', '#CAC7C7'),
(2, 2, 'CONFIRMACION DE PAGO', '#C6C4C4'),
(3, 3, 'GESTION BANCARIA', '#C1BFBF'),
(4, 4, 'CUENTA APERTURADA', '#B9B5B5 '),
(5, 5, 'ENVIO DE CODIGO AFILIADO', '#B2B1B1 '),
(6, 6, 'CODIGO AFILIADO BANCO', '#B0ABAB '),
(7, 7, 'ASIGNAR SERIAL', '#A9A8A8 '),
(8, 8, 'ENVIO CARGA PARAMETRO', '#A3A1A1 '),
(9, 9, 'ENVIO BANCO', '#A49D9D '),
(10, 10, 'RESPUESTA CCRD', '#A39898 '),
(11, 11, 'PARAMETRIZACION', '#9C9A9A '),
(12, 12, 'TRASLADO CARI', '#959393 '),
(13, 13, 'LISTO PARA ENTREGAR', '#979191 '),
(14, 14, 'RETIRADO POR CLIENTE', '#908F8F ');


ALTER TABLE `tblstatus_operations`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tblstatus_operations`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

---
--
--

CREATE TABLE `tbltype_operations` (
  `id` int(10) NOT NULL,
  `id_type` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;



INSERT INTO `tbltype_operations` (`id`, `id_type`, `name`) VALUES
(1, 1, 'INSTALACION'),
(2, 2, 'REACTIVACION'),
(3, 3, 'CAMBIO DE RAZON SOCIAL'),
(4, 4, 'CAMBIO BANCO'),
(5, 5, 'DESINSTALACION'),
(6, 6, 'CORRECTIVO'),
(7, 7, 'CAMBIO DE SIMCARD');


ALTER TABLE `tbltype_operations`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tbltype_operations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;


-- adaptacion de tabla operadoras

ALTER TABLE `tbloperadora` 
ADD COLUMN `coneccion` VARCHAR(12) NOT NULL DEFAULT '' AFTER `mostrar_en_simcard`;

UPDATE `tbloperadora` SET `coneccion` = 'INALAMBRICA' WHERE (`operadoraid` = '1');
UPDATE `tbloperadora` SET `coneccion` = 'INALAMBRICA' WHERE (`operadoraid` = '2');
UPDATE `tbloperadora` SET `coneccion` = 'INALAMBRICA' WHERE (`operadoraid` = '3');
UPDATE `tbloperadora` SET `coneccion` = 'ALAMBRICA' WHERE (`operadoraid` = '4');
UPDATE `tbloperadora` SET `coneccion` = 'ALAMBRICA' WHERE (`operadoraid` = '5');


INSERT INTO `tbloptions` (`id`, `name`, `value`, `autoload`) VALUES ('420', 'requests_number_prefix', 'SOL-', '1');

--- inserta registros de servicios en la tabla items 

INSERT INTO `tblitems` (`id`, `description`, `long_description`, `rate`, `tax`, `tax2`, `unit`, `group_id`, `commodity_code`, `commodity_barcode`, `commodity_type`, `warehouse_id`, `origin`, `color_id`, `style_id`, `model_id`, `size_id`, `unit_id`, `sku_code`, `sku_name`, `purchase_price`, `sub_group`, `commodity_name`, `color`, `dateReplica`, `status_profit`) VALUES

(66, 'Servicio Cambio de SimCard', 'Servicio Cambio de SimCard ', '25.00', 0, NULL, NULL, 1, '0102', '82718414798', 1, NULL, '', NULL, 0, 0, 0, 1, 'e-Ingenico-0066', 'Servicio Cambio de SimCard ', '25.00', '1', '', '', '2021-11-10 14:28:13', 0),
(67, 'Servicio Cambio de Razón Social', 'Servicio Cambio de Razón Social', '15.00', 0, NULL, NULL, 1, '0103', '82718414798', 1, NULL, '', NULL, 0, 0, 0, 1, 'e-Ingenico-0066', 'Servicio Cambio de Razón Social', '15.00', '1', '', '', '2021-11-10 14:28:13', 0),
(68, 'Servicio Cambio de Banco', 'Servicio Cambio de Banco', '15.00', 0, NULL, NULL, 1, '0104', '82718414798', 1, NULL, '', NULL, 0, 0, 0, 1, 'e-Ingenico-0066', 'Servicio Cambio de Banco', '15.00', '1', '', '', '2021-11-10 14:28:13', 0),
(60, 'Servicio Reactivación', 'Servicio Reactivación', '15.00', 0, NULL, NULL, 1, '0102', '82718414798', 1, NULL, '', NULL, 0, 0, 0, 1, 'e-Ingenico-0066', 'Servicio Reactivación', '15.00', '1', '', '', '2021-11-10 14:28:13', 0);

ALTER TABLE `tblterminals` 
ADD COLUMN `numterminal` INT(5) NULL DEFAULT NULL AFTER `banks`;

--
--
--
ALTER TABLE `tbloperations_dates` CHANGE `sale` `sale` DATE NULL DEFAULT NULL, CHANGE `payment_confirmed` `payment_confirmed` DATE NULL DEFAULT NULL, CHANGE `bank_management` `bank_management` DATE NULL DEFAULT NULL, CHANGE `account_opened` `account_opened` DATE NULL DEFAULT NULL, CHANGE `send_code_af` `send_code_af` DATE NULL DEFAULT NULL, CHANGE `bank_code_af` `bank_code_af` DATE NULL DEFAULT NULL, CHANGE `serial_assignment` `serial_assignment` DATE NULL DEFAULT NULL, CHANGE `send_par_loading` `send_par_loading` DATE NULL DEFAULT NULL, CHANGE `bank_par_loading` `bank_par_loading` DATE NULL DEFAULT NULL, CHANGE `ccrd_answer` `ccrd_answer` DATE NULL DEFAULT NULL, CHANGE `parameterization` `parameterization` DATE NULL DEFAULT NULL, CHANGE `cari_relocated` `cari_relocated` DATE NULL DEFAULT NULL, CHANGE `ready` `ready` DATE NULL DEFAULT NULL, CHANGE `retired` `retired` DATE NULL DEFAULT NULL;
--
--
--

ALTER TABLE `tblstatus_operations` ADD `day` INT(3) NOT NULL AFTER `color`;

INSERT INTO `tbltype_operations` (`id`, `id_type`, `name`) VALUES ('7', '7', 'CAMBIO DE SIMCARD');
