-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 17-08-2021 a las 14:30:22
-- Versión del servidor: 5.7.32
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblingenico`
--

CREATE TABLE `tblingenico` (
  `id` int(11) NOT NULL,
  `country` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `repair_center` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `customer_name` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `in_serial_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `out_serial_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `terminal_part_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `model` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `terminal_imei_address` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `terminal_mac_address` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `made_in` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `pci` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `sales_date` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `end_sales_warranty` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `date_of_customization` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `in_warranty` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `out_warranty` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `in_warranty_accesories` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `out_warranty_accesories` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `in_date` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `date_of_entry_to_repair` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `date_start_stop_terminal` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `date_end_stop_terminal` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `data_of_repair` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `out_date` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `deadline` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `report_month` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `backlog` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `time_of_repairclient_with_discount_supply` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `time_of_repairclient_with_discount_customer` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `sla_with_discount_cr` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `sla_with_discount_customer` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `mrr` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `bounce` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `date_of_the_last_repair` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `drr` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `doa_repair` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `eco_1` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `custom_doa` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `first_pass_yield_fpytest2` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `first_pass_yield_fpyqa` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `1_customer_symptom` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `2_customer_symptom` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `3_customer_symptom` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `4_customer_symptom` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `5_customer_symptom` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `1_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `2_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `3_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `4_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `5_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `6_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `7_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `8_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `9_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `10_technician_diagnosis` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `1_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `2_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `3_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `4_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `5_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `6_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `7_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `8_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `9_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `10_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `11_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `12_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `13_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `14_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `15_technician_resolution` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `1_part_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `1_part_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `1_part_description` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `1_part_cost` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `2_part_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `2_part_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `2_part_description` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `2_part_cost` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `3_part_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `3_part_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `3_part_description` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `3_part_cost` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `4_part_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `4_part_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `4_part_description` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `4_part_cost` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `5_part_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `5_part_number` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `5_part_description` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `5_part_cost` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `repair_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `repair_level` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `detail_repair_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `terminal_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `accesories_status` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `unit_cogs_USD` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `Logistics` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `other_cost` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `total_cogs_unit_cogs_Logistica_otros` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `workforce_cost` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `app` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `workforce_cost_total_work_app` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `administration_cost` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `external_services_cost_USD` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `comments1` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `comments2` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `comments3` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `comments4` varchar(90) COLLATE utf8mb4_spanish_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tickecid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tblingenico`
--

INSERT INTO `tblingenico` (`id`, `country`, `repair_center`, `customer_name`, `in_serial_number`, `out_serial_number`, `terminal_part_number`, `model`, `terminal_imei_address`, `terminal_mac_address`, `made_in`, `pci`, `sales_date`, `end_sales_warranty`, `date_of_customization`, `in_warranty`, `out_warranty`, `in_warranty_accesories`, `out_warranty_accesories`, `in_date`, `date_of_entry_to_repair`, `date_start_stop_terminal`, `date_end_stop_terminal`, `data_of_repair`, `out_date`, `deadline`, `report_month`, `backlog`, `time_of_repairclient_with_discount_supply`, `time_of_repairclient_with_discount_customer`, `sla_with_discount_cr`, `sla_with_discount_customer`, `mrr`, `bounce`, `date_of_the_last_repair`, `drr`, `doa_repair`, `eco_1`, `custom_doa`, `first_pass_yield_fpytest2`, `first_pass_yield_fpyqa`, `1_customer_symptom`, `2_customer_symptom`, `3_customer_symptom`, `4_customer_symptom`, `5_customer_symptom`, `1_technician_diagnosis`, `2_technician_diagnosis`, `3_technician_diagnosis`, `4_technician_diagnosis`, `5_technician_diagnosis`, `6_technician_diagnosis`, `7_technician_diagnosis`, `8_technician_diagnosis`, `9_technician_diagnosis`, `10_technician_diagnosis`, `1_technician_resolution`, `2_technician_resolution`, `3_technician_resolution`, `4_technician_resolution`, `5_technician_resolution`, `6_technician_resolution`, `7_technician_resolution`, `8_technician_resolution`, `9_technician_resolution`, `10_technician_resolution`, `11_technician_resolution`, `12_technician_resolution`, `13_technician_resolution`, `14_technician_resolution`, `15_technician_resolution`, `1_part_status`, `1_part_number`, `1_part_description`, `1_part_cost`, `2_part_status`, `2_part_number`, `2_part_description`, `2_part_cost`, `3_part_status`, `3_part_number`, `3_part_description`, `3_part_cost`, `4_part_status`, `4_part_number`, `4_part_description`, `4_part_cost`, `5_part_status`, `5_part_number`, `5_part_description`, `5_part_cost`, `repair_status`, `repair_level`, `detail_repair_status`, `terminal_status`, `accesories_status`, `unit_cogs_USD`, `Logistics`, `other_cost`, `total_cogs_unit_cogs_Logistica_otros`, `workforce_cost`, `app`, `workforce_cost_total_work_app`, `administration_cost`, `external_services_cost_USD`, `comments1`, `comments2`, `comments3`, `comments4`, `date`, `tickecid`) VALUES
(1, 'Venezuela', 'PAYTECH VE', '1', '12350WL39461970', '12350WL39461970', 'PR', '22', '321321', '231231', '3', 'PCI1', '16/12/2012', '16/12/2013', '#N/A', 'OW', 'OW', 'OW', 'OW', '11/08/2021', '11/08/2021', '12/08/2021', '12/08/2021', '12/08/2021', '11/08/2021', 'NUll', '13-08-2021', 'NO', '1', '1', '100%', '100%', 'NO', 'NO', '13-08-2021', 'NO', 'NO', 'NO', 'NO', '1', '1', 'C0001', 'NI', 'NI', 'NI', 'NI', '1', '1', '1', '1', '1', 'NI', 'NI', 'NI', 'NI', 'NI', '1', '1', '1', '1', '1', '6 Technician Resolution', 'NI', 'NI', 'NI', 'NI', 'NI', 'NI', 'NI', 'NI', 'NI', '', '  IPP315-31T3512A', 'IPP315 32+128 T U CSN SD 0N STD FR C', '0', '', '  ISC250-31T3751A', 'ISC 250 64 + 128 T MPT NCN SD 2 STD GC S', '0', '', '  TWF31311388R', 'MOV25Fx-128+256CL-xx3GWI-D20-NSSS-OO-SA', '0', '', ' IPP350-51T3332A', 'IPP350 32+128 T URE CPN SD 3S STD GC S', '0', '', ' ISC250-01T2394C', 'ISC 250 64+128 T MPT NEN SD 2 STD GC S', '0', 'repair_status', 'repair_level', 'detail_repair_status', 'Terminal_Status', 'Accesories_Status', 'Unit Cogs (USD)', 'Logistics', 'Other cost', 'Total Cogs Unit Cogs+Logistica+otros', 'Workforce Cost', 'app', 'Workforce Cost Total Work +App', 'Administration Cost', 'External Services Cost (USD)', 'comments1', 'comments2', 'comments3', 'comments4', '2021-08-12 12:57:19', 1267);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblingenico`
--
ALTER TABLE `tblingenico`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblingenico`
--
ALTER TABLE `tblingenico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
