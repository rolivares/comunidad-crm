ALTER TABLE `tbltickets_status` ADD `day` INT(255) NOT NULL AFTER `statusorder`;
ALTER TABLE `tbltickets` ADD `modification_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `date_warranty`;
ALTER TABLE `tbldepartments` ADD `day` INT(255) NOT NULL AFTER `hidefromclient`;

CREATE TABLE tbldepartments_status(
	id int(255) AUTO_INCREMENT,
    id_ticket int(255),
    laboratory DATE NOT NULL,
    administration_and_finance DATE NOT NULL,
    operations DATE NOT NULL,
    human_resources DATE NOT NULL,
    presidency DATE NOT NULL,
    callcenter DATE NOT NULL,
    store DATE NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE `tblstatus_operations` ADD `name_english` VARCHAR(255) NOT NULL AFTER `name`;
ALTER TABLE `tbldepartments` ADD `name_english` VARCHAR(255) NOT NULL AFTER `name`;




