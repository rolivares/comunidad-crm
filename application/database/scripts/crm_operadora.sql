CREATE TABLE `tbloperadora` (
  `operadoraid` int(11) NOT NULL,
  `operadora` varchar(11) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

INSERT INTO `tbloperadora` (`operadoraid`, `operadora`) VALUES
(1, 'Digitel'),
(2, 'Movistal'),
(3, 'Movilnet');

ALTER TABLE `tbloperadora`
  ADD PRIMARY KEY (`operadoraid`);

ALTER TABLE `tbloperadora`
  MODIFY `operadoraid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE tblterminals
 ADD FOREIGN KEY (operadoraid) REFERENCES tbloperadora(operadoraid);


ALTER TABLE `tblservices` ADD `coste` FLOAT NOT NULL AFTER `name`;
ALTER TABLE `tblservices` ADD `status` INT NOT NULL DEFAULT '1' AFTER `coste`;
