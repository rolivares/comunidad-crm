-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 04-01-2022 a las 17:05:39
-- Versión del servidor: 5.7.32
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblservices_tickets_level`
--

CREATE TABLE `tblservices_tickets_level` (
  `id` int(255) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblservices_tickets_level`
--

INSERT INTO `tblservices_tickets_level` (`id`, `level`, `status`) VALUES
(1, 'Reparado', 0),
(2, 'Inviable', 0),
(3, 'Diagnosticado', 0),
(4, 'Reparado/Sin Reactivar', 0),
(5, 'Solo Carga Aplicativo', 0),
(6, 'Personalizados', 0),
(7, 'Solo Reactivacion', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblservices_tickets_level`
--
ALTER TABLE `tblservices_tickets_level`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblservices_tickets_level`
--
ALTER TABLE `tblservices_tickets_level`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



UPDATE `tblstatus_operations` SET `name_english` = 'sale' WHERE `tblstatus_operations`.`id` = 1;
UPDATE `tblstatus_operations` SET `name_english` = 'payment_confirmed' WHERE `tblstatus_operations`.`id` = 2;
UPDATE `tblstatus_operations` SET `name_english` = 'bank_management' WHERE `tblstatus_operations`.`id` = 3;
UPDATE `tblstatus_operations` SET `name_english` = 'account_opened' WHERE `tblstatus_operations`.`id` = 4;
UPDATE `tblstatus_operations` SET `name_english` = 'send_code_af' WHERE `tblstatus_operations`.`id` = 5;
UPDATE `tblstatus_operations` SET `name_english` = 'bank_code_af' WHERE `tblstatus_operations`.`id` = 6;
UPDATE `tblstatus_operations` SET `name_english` = 'serial_assignment' WHERE `tblstatus_operations`.`id` = 7;
UPDATE `tblstatus_operations` SET `name_english` = 'send_par_loading' WHERE `tblstatus_operations`.`id` = 8;
UPDATE `tblstatus_operations` SET `name_english` = 'bank_par_loading' WHERE `tblstatus_operations`.`id` = 9;
UPDATE `tblstatus_operations` SET `name_english` = 'ccrd_answer' WHERE `tblstatus_operations`.`id` = 10;
UPDATE `tblstatus_operations` SET `name_english` = 'parameterization' WHERE `tblstatus_operations`.`id` = 11;
UPDATE `tblstatus_operations` SET `name_english` = 'cari_relocated' WHERE `tblstatus_operations`.`id` = 12;
UPDATE `tblstatus_operations` SET `name_english` = 'ready' WHERE `tblstatus_operations`.`id` = 13;
UPDATE `tblstatus_operations` SET `name_english` = 'retired' WHERE `tblstatus_operations`.`id` = 14;


UPDATE `tbldepartments` SET `name_english` = 'laboratory' WHERE `tbldepartments`.`departmentid` = 1;
UPDATE `tbldepartments` SET `name_english` = 'administration_and_finance' WHERE `tbldepartments`.`departmentid` = 2;
UPDATE `tbldepartments` SET `name_english` = 'operations' WHERE `tbldepartments`.`departmentid` = 3;
UPDATE `tbldepartments` SET `name_english` = 'human_resources' WHERE `tbldepartments`.`departmentid` = 4;
UPDATE `tbldepartments` SET `name_english` = 'presidency' WHERE `tbldepartments`.`departmentid` = 5;
UPDATE `tbldepartments` SET `name_english` = 'callcenter' WHERE `tbldepartments`.`departmentid` = 6;
UPDATE `tbldepartments` SET `name_english` = 'store' WHERE `tbldepartments`.`departmentid` = 7;