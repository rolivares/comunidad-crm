CREATE TABLE tblincidencia(
    id int(50) AUTO_INCREMENT PRIMARY KEY,
    incidents varchar(255)
);

INSERT INTO `tblincidencia` (`id`, `incidents`) VALUES 
(NULL, 'Aplicación dañadas'),
(NULL, 'Cara Feliz (pantalla azul o no avanza/se queda en LLT en pantalla)'),
(NULL, 'Cara triste (Alert Interruption/unauthorized no comunica)'),
(NULL, 'No transacciona'),
(NULL, 'No lee Smartcard'),
(NULL, 'No lee banda magnética'),
(NULL, 'Imprime rayas/Imprime borroso'),
(NULL, 'Módulo dañados'),
(NULL, 'Puertos dañados'),
(NULL, 'Terminal bloqueado'),
(NULL, 'Terminal no enciende'),
(NULL, 'Terminal roto'),
(NULL, 'Terminal no imprime'),
(NULL, 'Terminal con display dañado'),
(NULL, 'Terminal con pantalla en blanco'),
(NULL, 'Validación de SIM con carrier (GPRS)');