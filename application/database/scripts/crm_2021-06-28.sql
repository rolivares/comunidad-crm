CREATE TABLE `                                                                                                                                                                                                                                                                                                                                      ` (
  `id` int(3) NOT NULL,
  `id_payment` int(3) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tblstatus_payment`
--

INSERT INTO `tblstatus_payment` (`id`, `id_payment`, `name`) VALUES
(1, 1, 'POR VERIFICAR PAGO'),
(2, 2, 'VERIFICADO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblstatus_payment`
--
ALTER TABLE `tblstatus_payment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblstatus_payment`
--
ALTER TABLE `tblstatus_payment`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- ALTER TABLE `tblinvoicepaymentrecords`
--

ALTER TABLE `tblinvoicepaymentrecords` ADD `status_payment` INT(10) NOT NULL AFTER `transactionid`;