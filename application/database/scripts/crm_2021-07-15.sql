--
-- Estructura de tabla para la tabla `tblvisitor`
--

CREATE TABLE `tblvisitor` (
  `id_visitor` int(10) NOT NULL,
  `name_and_surname` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `surname` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `identification_card` int(10) NOT NULL,
  `admission_date` date NOT NULL,
  `business` varchar(500) COLLATE utf8mb4_spanish_ci NOT NULL,
  `rif` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `contact_number` int(30) NOT NULL,
  `place` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `check_in` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `sale_agent` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `attention_time` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `management_time` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `bank` int(10) NOT NULL,
  `fountain` int(10) NOT NULL,
  `reason_fo_visit` int(10) NOT NULL,
  `description` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblvisitor`
--
ALTER TABLE `tblvisitor`
  ADD PRIMARY KEY (`id_visitor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblvisitor`
--
ALTER TABLE `tblvisitor`
  MODIFY `id_visitor` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tblvisitor` CHANGE `contact_number` `contact_number` VARCHAR(30) NOT NULL;

--
-- Estructura de tabla para la tabla `tbltyping_visitor`
--

CREATE TABLE `tbltyping_visitor` (
  `id` int(10) NOT NULL,
  `typing` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tbltyping_visitor`
--

INSERT INTO `tbltyping_visitor` (`id`, `typing`) VALUES
(1, 'EQUIPO NUEVO'),
(2, 'EQUIPO REFURBISHED'),
(3, 'PERIFERICOS'),
(4, 'DOCUMENTOS'),
(5, 'CAMBIO DE RAZON SOCIAL'),
(6, 'CAMBIO SIM'),
(7, 'DESAFILIACION'),
(8, 'CONFIGURACION DE EQUIPO'),
(9, 'CAMBIO DE BANCO'),
(10, 'CAMBIO DE SIMCARD'),
(11, 'PRODUCTOS Y SERVICIOS'),
(12, 'PROCESOS'),
(13, 'ESTATUS EQUIPO'),
(14, 'DOMICILIACION'),
(15, 'DESINSTALACION'),
(16, 'EQUIPO REPARACION'),
(17, 'COMPRA DE EQUIPO'),
(18, 'SERVICIO'),
(19, 'SUSTITUCION');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbltyping_visitor`
--
ALTER TABLE `tbltyping_visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbltyping_visitor`
--
ALTER TABLE `tbltyping_visitor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;


COMMIT;