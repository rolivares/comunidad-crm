CREATE TABLE `tblaccounts` (
  `id` int(11) NOT NULL,
  `userid` int(6) NOT NULL,
  `naccount` int(20) NOT NULL,
  `codaffiliate` int(8) NOT NULL,
  `bankid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;


INSERT INTO `tblaccounts` (`id`, `userid`, `naccount`, `codaffiliate`, `bankid`) VALUES
(1, 1588, 1232412512, 80972253, 2);

ALTER TABLE `tblaccounts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tblaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;