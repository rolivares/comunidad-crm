CREATE TABLE `tbltype_rif` (
  `id` int(5) NOT NULL COMMENT 'id incremental',
  `type_rif` varchar(2) NOT NULL COMMENT 'Tipo de rif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbltype_rif` (`id`, `type_rif`) VALUES
(1, 'V'),
(2, 'G'),
(3, 'J'),
(4, 'P'),
(5, 'E');

ALTER TABLE `tbltype_rif`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbltype_rif`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'id incremental', AUTO_INCREMENT=6;
COMMIT;
