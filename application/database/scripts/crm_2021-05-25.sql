ALTER TABLE `tbltickets` ADD `sales_date` DATETIME NULL DEFAULT NULL AFTER `date`;
ALTER TABLE `tbltickets` ADD `date_warranty` DATETIME NULL DEFAULT NULL AFTER `sales_date`;
ALTER TABLE `tblingenico` ADD `tickecid` INT NOT NULL AFTER `date`;