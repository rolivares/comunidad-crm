

CREATE TABLE `tblsimcard_status` (
  `id` int(11) NOT NULL,
  `name_status` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;



INSERT INTO `tblsimcard_status` (`id`, `name_status`) VALUES
(1, 'Activa'),
(2, 'Inactiva');


ALTER TABLE `tblsimcard_status`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tblsimcard_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
