CREATE TABLE `tblmessage` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_spanish2_ci NOT NULL,
  `description` text COLLATE utf8_spanish2_ci NOT NULL,
  `date` date NOT NULL,
  `date_edition` date NOT NULL,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(30) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE `tblmessage_comments` (
  `id` int(7) NOT NULL,
  `content` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `message_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `dateadd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
