-- creaacion de tablas y datos iniciales para modulo de solicitudes

CREATE TABLE `tbloperations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `communication_required` int(11) DEFAULT NULL,
  `operator_required` int(11) DEFAULT NULL,
  `model_required` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `terminal` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `affiliate_code` int(11) DEFAULT NULL,
  `num_terminal` int(11) DEFAULT NULL,
  `sales_document` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `detail` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `observation` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `manage_code_af` int(11) DEFAULT NULL,
  `chanel` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `registration_user` int(11) NOT NULL,
  `registration_date` date NOT NULL,
  `modified_user` int(11) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `retired_by` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE `tbloperations_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iperation_id` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `sale` datetime DEFAULT NULL,
  `payment` datetime DEFAULT NULL,
  `send_code_af` datetime DEFAULT NULL,
  `bank_code_af` datetime DEFAULT NULL,
  `serial_ assignment` datetime DEFAULT NULL,
  `send_par_loading` datetime DEFAULT NULL,
  `bank_par_loading` datetime DEFAULT NULL,
  `ccrd_answer` datetime DEFAULT NULL,
  `parameterization` datetime DEFAULT NULL,
  `cari_relocated` datetime DEFAULT NULL,
  `tbloperations_datescol` datetime DEFAULT NULL,
  `revision` datetime DEFAULT NULL,
  `ready` datetime DEFAULT NULL,
  `retired` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


CREATE TABLE `tbloperation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


CREATE TABLE `tblrequirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bank` int(11) NOT NULL,
  `operation_type` int(11) NOT NULL,
  `requirement` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `required` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='Requisitos necesarios para las diferentes operaciones o solucitudes( instalación, desistalacion, etc)';