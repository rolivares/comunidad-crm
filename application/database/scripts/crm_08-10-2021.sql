CREATE TABLE `tblcustomer_name` (
  `id` int(11) NOT NULL,
  `customer` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO `tblcustomer_name` (`id`, `customer`) VALUES
(1, 'BANCO DEL TESORO VE'),
(2, 'BANESCO VE'),
(3, 'BNC VE'),
(4, 'CLIENTE COMENRCIO VE'),
(5, 'CREDICARD POS VE'),
(6, 'MOVIL SERVI,VE'),
(7, 'BNC VE'),
(8, 'PAYTECH VE'),
(9, 'SERVICIOS GENERALES VE'),
(10, 'SERVITECH VE'),
(11, 'THUNDERNET VE'),
(12, 'VAT-C VE'),
(13, 'VE-PAGOS VE');