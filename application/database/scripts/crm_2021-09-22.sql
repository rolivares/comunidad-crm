UPDATE `tbltickets_status` SET `name` = 'Diagnosticado' WHERE `tbltickets_status`.`ticketstatusid` = 4;

INSERT INTO `tbltickets_status` (`ticketstatusid`, `name`, `isdefault`, `statuscolor`, `statusorder`) VALUES (NULL, 'Pago Diagnosticado', '1', '#84c529', '9');

INSERT INTO `tbltickets_status` (`ticketstatusid`, `name`, `isdefault`, `statuscolor`, `statusorder`) VALUES (NULL, 'Reparado', '1', '#84c529', '10');

INSERT INTO `tbltickets_status` (`ticketstatusid`, `name`, `isdefault`, `statuscolor`, `statusorder`) VALUES (NULL, 'Entregado', '1', '#84c529', '11');

ALTER TABLE `tblterminal_models` ADD `marca` VARCHAR(250) NOT NULL AFTER `model`;

INSERT INTO `tblterminal_models` (`modelid`, `model`, `marca`, `conexiontype`, `status`, `datecreated`, `mostrar_en_terminals`) VALUES (NULL, 'NEWLAND', 'SP600', 'N/A', '1', '2021-09-22 13:41:12', '1'), (NULL, 'TOPWISE', 'T1', 'N/A', '1', '2021-09-22 13:41:12', '1');

UPDATE `tblterminal_models` SET `marca` = 'Ingenico' WHERE `tblterminal_models`.`marca` = '';