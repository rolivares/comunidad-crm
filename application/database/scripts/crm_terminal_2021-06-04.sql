CREATE TABLE `tblbanks` (
  `id` int(11) NOT NULL,
  `banks` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL,
  `codbank` varchar(4) COLLATE utf8mb4_spanish_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO `tblbanks` (`id`, `banks`, `codbank`, `status`) VALUES
(1, 'Banco de Venezuela, S.A.C.A.', '0102', 1),
(2, 'Venezolano de Crédito', '0104', 0),
(3, 'Mercantil', '0105', 0),
(4, 'Provincial', '0108', 0),
(5, 'Bancaribe', '0114', 0),
(6, 'Exterior', '0115', 0),
(7, 'Occidental de Descuento', '0116', 0),
(8, 'Banco Caroní', '0128', 1),
(9, 'Banesco', '0134', 0),
(10, 'Banco Plaza', '0138', 1),
(11, 'BFC Banco Fondo Común', '0151', 1),
(12, '100% Banco', '0156', 1),
(13, 'Del Sur', '0157', 1),
(14, 'Banco del Tesoro', '0163', 1),
(15, 'Banco Agrícola de Venezuela', '0166', 0),
(16, 'Bancrecer', '0168', 0),
(17, 'Mi Banco', '0169', 1),
(18, 'Bancamiga', '0172', 0),
(19, 'Banplus', '0174', 1),
(20, 'Bicentenario del Pueblo', '0175', 0),
(21, 'Banfanb', '0177', 0),
(22, 'BNC Nacional de Crédito', '0191', 0);

ALTER TABLE `tblbanks`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tblbanks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

CREATE TABLE `tblsimcard` (
  `id` int(11) NOT NULL,
  `serial` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `operadora` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

ALTER TABLE `tblsimcard`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tblsimcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

ALTER TABLE `tbltickets` ADD `resumetickets` mediumtext NOT NULL;
ALTER TABLE `tbltickets` CHANGE `ticketid` `ticketid` INT(11) AUTO_INCREMENT;

CREATE TABLE `tblterminal_models` (
  `modelid` int(11) NOT NULL,
  `model` varchar(300) NOT NULL,
  `conexiontype` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `datecreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tblterminal_models` (`modelid`, `model`, `conexiontype`, `status`, `datecreated`) VALUES
(1, 'iCT220', 'Dial up/Lan', 1, '2021-03-25 14:39:51'),
(2, 'iCT250', 'Dial up/Lan', 1, '2021-03-25 14:39:51'),
(3, 'iWL250', 'Inalambrico', 1, '2021-03-25 14:39:56'),
(4, 'iWL220', 'Inalambrico', 1, '2021-03-25 14:39:56');

ALTER TABLE `tblterminal_models`
  ADD PRIMARY KEY (`modelid`);

ALTER TABLE `tblterminal_models`
  MODIFY `modelid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

CREATE TABLE `tblterminals` (
  `id` int(11) NOT NULL,
  `userid` int(6) NOT NULL,
  `modelid` int(2) NOT NULL,
  `codaffiliate` int(8) NOT NULL,
  `serial` varchar(15) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `mac` varchar(30) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `imei` varchar(30) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `simcard` varchar(30) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `operadoraid` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `datecreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `banks` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

INSERT INTO `tblterminals` (`id`, `userid`, `modelid`, `codaffiliate`, `serial`, `mac`, `imei`, `simcard`, `operadoraid`, `status`, `datecreated`, `banks`) VALUES
(1, 1588, 1, 80972253, '17289WL26760719', '', '', '', 1, 1, '2021-03-26 12:28:36', 1),
(4, 1, 1, 82758950, '19193CT27156861', '53515362', '2145252512', '3151546632', 1, 1, '2021-04-05 11:05:25', 1),
(5, 1, 2, 82734937, '11095CT30447703', '346462', '24152151', '644662', 2, 1, '2021-04-05 11:07:59', 1);

ALTER TABLE `tblterminals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `banks` (`banks`),
  ADD KEY `modelid` (`modelid`);

ALTER TABLE `tblterminals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `tblterminals`
  ADD CONSTRAINT `tblterminals_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `tblclients` (`userid`),
  ADD CONSTRAINT `tblterminals_ibfk_2` FOREIGN KEY (`banks`) REFERENCES `tblbanks` (`id`),
  ADD CONSTRAINT `tblterminals_ibfk_3` FOREIGN KEY (`modelid`) REFERENCES `tblterminal_models` (`modelid`);