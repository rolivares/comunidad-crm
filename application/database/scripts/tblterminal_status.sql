

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";



CREATE TABLE `tblterminal_status` (
  `id` int(11) NOT NULL,
  `name_status` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


INSERT INTO `tblterminal_status` (`id`, `name_status`) VALUES
(1, 'Asignada'),
(2, 'Desintalada'),
(3, 'Con carga de llave'),
(4, 'Sin carga de llave');


ALTER TABLE `tblterminal_status`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tblterminal_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;
