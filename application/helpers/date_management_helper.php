<?php

defined('BASEPATH') or exit('No direct script access allowed');




function fields_of_operation_days($status = 1, $table = '')
{
    $CI  = &get_instance();
    $CI->db->select('name_english');
    $CI->db->from(db_prefix() . $table);
    $result = $CI->db->get()->result_array();
    return $result[$status]['name_english'];
}

function update_date($_D)
{
    do {
        if ($_D['status'] != '') {
            $_D['arr'][$_D['status']] =
                $_D['campos'][$_D['status']] != NULL &&
                $_D['campos'][$_D['status']] != '0000-00-00'
                ? $_D['campos'][$_D['status']]
                : date('Y-m-d');
        }
        $_D['status'] = fields_of_operation_days($_D['Current_status'], $_D['table']);
        $_D['Current_status']++;
    } while ($_D['status'] !== $_D['Change_status']);

    $_status = $_D['Change_status'];
    $_D['arr'][$_status] = date('Y-m-d');  // Añadimos la fecha actual al status seleccionado

    return $_D['arr'];
}

function reverse_date($_D)
{
    do {
        if ($_D['status'] != '') {
            if (
                $_D['campos'][$_D['status']] != NULL &&
                $_D['campos'][$_D['status']] != '0000-00-00'
            ) {
                $_D['arr'][$_D['status']] = NULL;
            }
        }
        $_D['status'] = fields_of_operation_days($_D['Current_status'], $_D['table']);
        $_D['Current_status']--;
    } while ($_D['status'] !== $_D['Change_status']);

    $_status = $_D['Change_status'];
    $_D['arr'][$_status] = date('Y-m-d'); // Añadimos la fecha actual al status seleccionado

    return $_D['arr'];
}

function date_management($option = [])
{
    if (!empty($option['case'])) {

        $_Closure = [
            'operations' => function () use ($option) {
                $fun = [
                    'Update' => fn ($_D) => update_date($_D),
                    'Reverse' => fn ($_D) => reverse_date($_D),
                ];
                return array_replace(
                    $option['campos'],
                    $fun[$option['opt']]($option)
                );
            },
            'department' => function () use ($option) {
                $fun = [
                    'Update' => fn ($_D) => update_date($_D),
                    'Reverse' => fn ($_D) => reverse_date($_D),
                ];
                return array_replace(
                    $option['campos'],
                    $fun[$option['opt']]($option)
                );
            },
        ];

        $result = $_Closure[$option['case']]();

        return $result;
    } else {
        return false;
    }
}
