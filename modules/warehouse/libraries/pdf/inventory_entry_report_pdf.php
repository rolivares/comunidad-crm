<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH . 'libraries/pdf/App_pdf.php';

/**
 * stock_inventory_entry_report pdf
 */
class Stock_inventory_entry_report_pdf extends App_pdf {
	protected $stock_inventory_entry_view;
	public $font_size = '';
	public $size = 9;

	/**
	 * get font size
	 * @return
	 */
	public function get_font_size() {
		return $this->font_size;
	}

	/**
	 * set font size
	 * @param
	 */
	public function set_font_size($size) {
		$this->font_size = 8;

		return $this;
	}

	/**
	 * construct
	 * @param
	 */
	public function __construct($stock_inventory_entry_view) {

		$stock_inventory_entry_view = hooks()->apply_filters('request_html_pdf_data', $stock_inventory_entry_view);
		$GLOBALS['stock_inventory_entry_view_report_pdf'] = $stock_inventory_entry_view;

		parent::__construct();

		$this->stock_inventory_entry_view = $stock_inventory_entry_view;

		$this->SetTitle('stock_inventory_entry_view');

		# Don't remove these lines - important for the PDF layout
		$this->stock_inventory_entry_view = $this->fix_editor_html($this->stock_inventory_entry_view);
	}

	/**
	 * prepare
	 * @return
	 */
	public function prepare() {
		$this->set_view_vars('stock_inventory_entry_view', $this->stock_inventory_entry_view);

		return $this->build();
	}

	/**
	 * type
	 * @return
	 */
	protected function type() {
		return 'stock_inventory_entry_view';
	}

	/**
	 * file path
	 * @return
	 */
	protected function file_path() {
		$customPath = APPPATH . 'views/themes/' . active_clients_theme() . '/views/my_requestpdf.php';
        $actualPath = APP_MODULES_PATH . '/warehouse/views/report/inventory_entry_reportpdf.php';
        
		if (file_exists($customPath)) {
			$actualPath = $customPath;
		}

		return $actualPath;
	}
}