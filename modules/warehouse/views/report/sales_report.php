<div class="col-md-12">
    <?php echo form_open_multipart(admin_url('warehouse/inventory_entry_pdf'), array('id' => 'inventory_entry_repor')); ?>

    <div class="col-md-12 pull-right">
        <div class="col-md-6">
            <input type="checkbox" name="expandir_rango" value="1" onclick="expandir_formulario('rango')">
            <?php echo _l('by_date_range'); ?>
            <div id='rangofecha' style="display: none;">
                <div class="col-md-4">
                    <?php echo render_date_input('from_date', 'from_date', date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))); ?>
                </div>

                <div class="col-md-4">
                    <?php echo render_date_input('to_date', 'to_date', date('Y-m-d')); ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <input type="checkbox" name="expandir_estatus" value="1" onclick="expandir_formulario('status')">
            <?php echo _l('by_status'); ?>
            <div id="status" style="display: none;">
                <div class="col-md-4">
                    <?php echo render_select('status_', $status, array('id_status', 'name'),'status'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-2 button-pdf-margin-top">
            <div class="form-group">
                <div class="btn-group">
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-pdf-o"></i><?php if (is_mobile()) {
                                                                                                                                                                                echo ' PDF';
                                                                                                                                                                            } ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="hidden-xs"><a href="?output_type=I" target="_blank" onclick="stock_submit(this); return false;"><?php echo _l('download_pdf'); ?></a></li>

                    </ul>
                    <br>
                    <a href="#" onclick="get_sales_report(); return false;" style="margin-top: 1em;" class="btn btn-info display-block button-pdf-margin-top"><?php echo _l('ok'); ?></a>
                </div>
            </div>
        </div>
        
    </div>
  


    <?php echo form_close(); ?>
</div>
<hr class="hr-panel-heading" />
<div class="col-md-12" id="report">
    <div class="panel panel-info col-md-12 panel-padding">

        <div class="panel-body" id="inventory_entry_report">
            <p>
                <h3 class="bold text-center"><?php echo mb_strtoupper(_l('sales_report')); ?></h3>
            </p>
            </div-->

            <div id="inventorySales" class="table-responsive">
                <table class="table table-striped" id="tblinventorySales">
                    <thead>
                        <tr>

                            <th colspan="2"><?php echo _l('invoice_number') ?></th>
                            <th colspan="2"><?php echo _l('date') ?></th>
                            <th colspan="2"><?php echo _l('client') ?></th>
                            <th colspan="2"><?php echo _l('due_date') ?></th>
                            <th colspan="2"><?php echo _l('status') ?></th>
                        </tr>
                    </thead>

                    <tbody class="list">
                        <tr>
                            <td colspan="2" id="invoice_number">.....</td>
                            <td colspan="2" id="date">.....</td>
                            <td colspan="2" id="client">.....</td>
                            <td colspan="2" id="due_date">.....</td>
                            <td colspan="2" id="status">.....</td>
                        </tr>
                    </tbody>
                </table>

                <ul class="pagination text-right"></ul>
            </div>


        </div>
    </div>
    <?php require 'modules/warehouse/assets/js/sales_report.php'; ?>
    <!--     <script src="modules/warehouse/assets/js/list.min.js"></script>
 -->
</div>