<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper" >
   <div class="content">
         <div class="col-md">
            <div class="panel_s">
               <div class="panel-body">
                  <div>
                     <div class="tab-content">
                     <h4 class="customer-profile-group-heading"><?php echo _l('import_profit_item'); ?></h4>
                        <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo render_input( 'product_code', 'product_code','','text','','','','','product_code'); ?>
                                    </div>
                                </div>
                             
                                <div class="modal-footer">
                                    <!--button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button-->
                                    <button href="#" onclick="setitem(); return false;" style="margin-top: 1em;" class="btn btn-info"><?php echo _l('import'); ?></button>
                                </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<?php require 'modules/warehouse/assets/js/import_item_profit_js.php' ?>
</div>