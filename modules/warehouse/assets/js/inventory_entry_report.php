  <script>
    function get_data_inventory_entry_report() {
      "use strict";
      var formData = new FormData();

      formData.append("csrf_token_name", $('input[name="csrf_token_name"]').val());
      if ($("[name='expandir_rango']").prop("checked")==true){
        formData.append("from_date", $('input[name="from_date"]').val());
        formData.append("to_date", $('input[name="to_date"]').val());
      }
      if ($("[name='expandir_almacen']").prop("checked")==true){
        formData.append("warehouse_id", $('select[name="warehouse_id"]').val());;
      }



      $.ajax({
        url: admin_url + 'warehouse/get_data_inventory_entry_report',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false
      }).done(function(response) {
        var response = JSON.parse(response);
        var trs = [];
        response.value.forEach(function(value) {
          var tr = $("<tr>");
          tr.html([
            $('<td colspan="1">').html($("<a href='<?php echo admin_url("warehouse/inventory_entry/") ?>" + value.id + "'>").html(value.goods_receipt_code)),
            $('<td colspan="1">').text(value.supplier_name),
            $('<td colspan="1">').text(value.firstname),
            $('<td colspan="2">').text(value.total_tax_money),
            $('<td colspan="2">').text(value.total_goods_money),
            $('<td colspan="2">').text(value.value_of_inventory),
            $('<td colspan="2">').text(value.total_money)
          ]);
          trs.push(tr);
        });
        $("#inventory_entry_report table tbody").html(trs);

        var monkeyList = new List('inventoryEntryReport', {
          valueNames: ['name'],
          page: 5,
          pagination: true
        });

      });

    }

    function expandir_formulario(type) {

      if (type == 'rango') {
        var x = document.getElementById("rangofecha");
        if (x.style.display === "none") {
          x.style.display = "block";
        } else {
          x.style.display = "none";
        }
      }
      if (type == 'almacen') {
        var x = document.getElementById("almacen");
        if (x.style.display === "none") {
          x.style.display = "block";
        } else {
          x.style.display = "none";
        }
      }
    }

    function stock_submit(invoker) {
      "use strict";
      $('#inventory_entry_report').submit();
    }
  </script>