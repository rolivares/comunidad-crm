<script>
  function get_sales_report() {
    "use strict";
    var formData = new FormData();

    formData.append("csrf_token_name", $('input[name="csrf_token_name"]').val());
    if ($("[name='expandir_rango']").prop("checked")==true){
      formData.append("from_date", $('input[name="from_date"]').val());
      formData.append("to_date", $('input[name="to_date"]').val());
    }
    if ($("[name='expandir_estatus']").prop("checked")==true){
      formData.append("status_", $('select[name="status_"]').val());
    }
    
    $.ajax({
      url: admin_url + 'warehouse/get_sales_report',
      method: 'post',
      data: formData,
      contentType: false,
      processData: false
    }).done(function(response) {

      var response = JSON.parse(response);
      console.log(response);
      var trs = [];
      response.value.forEach(function(value) {
        var tr = $("<tr>");
        tr.html([
          $('<td colspan="2">').html($("<a href='<?php echo base_url("invoice/") ?>" + value.number + "/" + value.hash + "'>").html(value.factura)),
          $('<td colspan="2">').text(value.datecreated),
          $('<td colspan="2">').text(value.company),
          $('<td colspan="2">').text(value.duedate),
          $('<td colspan="2">').text(value.status)
        ]);
        trs.push(tr);
      });
      $("#inventory_entry_report table tbody").html(trs);

      var monkeyList = new List('inventorySales', {
        valueNames: ['name'],
        page: 5,
        pagination: true
      });

    });
  }


  function expandir_formulario(type) {

    if (type =='rango'){
      var x = document.getElementById("rangofecha");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    if (type =='status'){
      var x = document.getElementById("status");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
  }



  function stock_submit(invoker) {
    "use strict";
    $('#inventory_entry_repor').submit();
  }
</script>